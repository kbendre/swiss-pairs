/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Kaustubh Bendre
 */


public class SwissPairs extends Application {
    final static int MAX_PAIRS = 101; //Max Pairs = 100
    public static TabPane tabPane = new TabPane();
    public static String[][] pairnames;// = new String [100][3];
    public static Tab namestab = new Tab();
    public static Tab displaytab = new Tab();
    public static Tab maintab = new Tab();
    
    public static int pairs;
    public static String nameOfEvent;
    public static LocalDate date;
    public static int bdsperrnd;
    public static int tables;
    public static Boolean usingBM = false;
    public static int[] firstbd = new int[31]; //Max Rounds = 30
    public static String dbfile = "sp.bws";
    public static String bmpropath = "C:\\Program Files\\Bridgemate Pro";
    public static String homepath = "C:\\SwissPairs\\";    
    public static String drawfilename;
    public static String rankingfilename;
    public static String dealfilename = "swiss.pbn";
    
    public static double[][] crosstable = new double[MAX_PAIRS][MAX_PAIRS];
    public static int[] leaders = new int[MAX_PAIRS]; //[pair no] sorted by vp score
    public static double[] penalties = new double[MAX_PAIRS];
    public static int[] newdraw;// = new int[pairs + 1];
    public static Boolean[] withdrawn = new Boolean[MAX_PAIRS];
    
    public static int withdrawnpairs = 0;
    public static Boolean lastround = false;
    
    public static RoundScoreSheet[] mss = new RoundScoreSheet[31]; //Max Rounds = 30
    public static int drawhistory[][] = new int[MAX_PAIRS][31]; 
    public static int rndsScored = 0;
    
    public static int editscore_bdno = 0;//hack for editing score
    public static int editscore_score = 0;//hack for editing score
    
    public static Text t4 = new Text("");
    public static Text t5 = new Text("");   
    
    
    @Override
    public void start(Stage primaryStage) {
        
        maintab.setText("Main");
        maintab.setClosable(false);
        maintab.setContent(setinitContent(primaryStage));
        tabPane.getTabs().add(maintab);         
        
        namestab.setText("Names");
        namestab.setClosable(false);       
        namestab.setDisable(true);
        
        displaytab.setText("Scores");
        displaytab.setClosable(false);       
        displaytab.setDisable(true);
        
        tabPane.getTabs().add(namestab);
        tabPane.getTabs().add(displaytab);
        
        Scene scene = new Scene(tabPane, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Swiss Pairs");            
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static GridPane setinitContent(Stage primaryStage){
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Set Parameters:");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label nameOfEventlabel = new Label("Name of Event:");
        grid.add(nameOfEventlabel, 0, 1);

        TextField nameOfEventTextField = new TextField("Deccan Club Swiss Pairs");
        nameOfEventTextField.setPrefWidth(250);
        grid.add(nameOfEventTextField, 1, 1);

        Label datelabel = new Label("Date:");
        grid.add(datelabel, 0, 2);

        final DatePicker datePicker = new DatePicker(LocalDate.now());
        grid.add(datePicker, 1, 2);

        Label noOfPairslabel = new Label("Number of Pairs:");
        grid.add(noOfPairslabel, 0, 3);

        Spinner<Integer> noofpairsSpinner = new Spinner(6, 100, 20);
        grid.add(noofpairsSpinner, 1, 3);
        
        Label bdsperRoundlabel = new Label("Boards per Round:");
        grid.add(bdsperRoundlabel, 0, 4);

        Spinner<Integer> bdsperRoundSpinner = new Spinner(4, 10, 4);
        grid.add(bdsperRoundSpinner, 1, 4);
        
        Label homepathlabel = new Label("Home Folder:");
        grid.add(homepathlabel, 0, 5);

        Text homepathtext = new Text("C:\\SwissPairs\\");
       // grid.add(homepathtext, 1, 5);
        
        Button homepathbtn = new Button("Change");
        //grid.add(homepathbtn, 2, 5);
        homepathbtn.setOnAction ((ActionEvent e) -> {
            DirectoryChooser dc = new DirectoryChooser();
            dc.setInitialDirectory(new File("C:\\"));
            File file = dc.showDialog(primaryStage);
           // System.out.println(file);
            homepath = file + "\\";
            homepathtext.setText(homepath);
           // System.out.println(homepath);
        });
        
        HBox homepathhb = new HBox();
        homepathhb.setSpacing(20.0);
        homepathhb.setPadding(new Insets(25, 25, 25, 25));
        homepathhb.getChildren().addAll(homepathtext,homepathbtn);
        homepathhb.setAlignment(Pos.CENTER_LEFT);
                
        grid.add(homepathhb, 1, 5);
        
        ToggleGroup bmgroup = new ToggleGroup();
        RadioButton usebm = new RadioButton("Yes");
        RadioButton donotusebm = new RadioButton("No");
         
        usebm.setToggleGroup(bmgroup);
        donotusebm.setToggleGroup(bmgroup);
        usebm.setSelected(true);
        
        
        Label usebmlabel = new Label("Use Bridgemates? ");
        grid.add(usebmlabel, 0, 6);
        HBox usebmhbox = new HBox();
        usebmhbox.setSpacing(15);
        usebmhbox.getChildren().addAll(usebm,donotusebm);
        grid.add(usebmhbox, 1, 6);
        
        
        
        Label bmpathlabel = new Label("BMPro Folder:");
        grid.add(bmpathlabel, 0, 8);
        
        Text bmpathtext;
        File bmfile = new File("C:\\Program Files\\Bridgemate Pro\\BMPro.exe");
        if (bmfile.exists()){
            bmpathtext = new Text("C:\\Program Files\\Bridgemate Pro\\BMPro.exe");
            bmpropath = "C:\\Program Files\\Bridgemate Pro\\";
        }
        else {
            bmpathtext = new Text("C:\\Program Files (x86)\\Bridgemate Pro\\BMPro.exe");
            bmpropath = "C:\\Program Files (x86)\\Bridgemate Pro\\";
        }
       
        grid.add(bmpathtext, 1, 8);
        
        Button btn = new Button();
        btn.setText("Start"); 
        btn.setMinSize(100,100);
        grid.add(btn, 0, 11);
        btn.setOnAction((ActionEvent event) -> {
            nameOfEvent = nameOfEventTextField.getText();
            date = datePicker.getValue();
            pairs = noofpairsSpinner.getValue();
            bdsperrnd = bdsperRoundSpinner.getValue();
            usingBM = usebm.isSelected();
            
            //SwissPairs.bmpropath = pathTextField.getText();
            pairnames = new String[100][3];
            newdraw = new int[pairs + 1];
            for(int i=0; i<=pairs; i++) newdraw[i] = i;
            tables = pairs%2 == 1 ? (pairs-1)/2 : pairs/2;
            //btn_start_new_round.setDisable(false);
            Arrays.fill(withdrawn, Boolean.FALSE);
            
            namestab.setContent(Names.Names());
            namestab.setDisable(false);
            
            maintab.setContent(setContent());
           // bdsperRoundSpinner.setDisable(true);
            //noofpairsSpinner.setDisable(true);
            //btn.setDisable(true);
        });
        return grid;

    }
    public static GridPane setContent(){
        int bds = SwissPairs.bdsperrnd;
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text t1 = new Text(nameOfEvent);
        t1.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(t1, 0, 0, 2, 1);
      
        Text t2 = new Text("Number of Pairs: " + pairs);
        t2.setFont(Font.font("Tahoma", FontWeight.NORMAL, 15));
        
        Text t3 = new Text("Boards per Round: " + bdsperrnd);
        t3.setFont(Font.font("Tahoma", FontWeight.NORMAL, 15));
        
        //t4 = new Text("");
        t4.setFont(Font.font("Tahoma", FontWeight.NORMAL, 15));
        
        //t5 = new Text("");
        t5.setFont(Font.font("Tahoma", FontWeight.NORMAL, 15));
        
        
        
        Text t6 = new Text("Round " + (rndsScored + 1) + ":");
        t6.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        
        Label l1 = new Label("First Board:");        

        int n = (32/bds - 1) * bds + 1;        
        Spinner<Integer> fbdSpinner = new Spinner(1, n, SwissPairs.rndsScored*bds + 1, bds);
        fbdSpinner.setPrefWidth(60);
        
        Button bmbtn = new Button();
        Button sbtn = new Button();
       // Button stbtn = new Button();
        Button makedrawbtn = new Button();
        Button rankingbtn = new Button();
        Button summbtn = new Button();
        Button editscoresbtn = new Button();
        Button adjbtn = new Button();
        Button removepairbtn = new Button();
        Button restartBMbtn = new Button();
        
        if(usingBM){
            if(SwissPairs.rndsScored == 0) bmbtn.setText("Start BridgeMates");
            else bmbtn.setText("Update BridgeMates");
        }
        else bmbtn.setText("Update Score Entry File");
        
        bmbtn.setDisable(true);
        bmbtn.setPrefWidth(200);            
        bmbtn.setOnAction((ActionEvent event) -> {
            try {
                SwissPairs.firstbd[SwissPairs.rndsScored + 1] = fbdSpinner.getValue();             
                //Draw.Draw();   
                if(usingBM){
                    BM.initiateDB();
                    if(SwissPairs.rndsScored == 0){                    
                  //      Process process = new ProcessBuilder(SwissPairs.bmpropath + "\\BMPro.exe", "/f:[" + 
                  //                                       SwissPairs.homepath + dbfile + "]/s/r").start();
                    }
                    restartBMbtn.setDisable(false);
                }
                else Score.editScoreFile(SwissPairs.rndsScored + 1);
                makedrawbtn.setDisable(true);
                rankingbtn.setDisable(true);               
                removepairbtn.setDisable(true);
                bmbtn.setDisable(true);                
                sbtn.setDisable(false);
                
            } catch (ClassNotFoundException | SQLException | IOException ex) {
           // catch (ClassNotFoundException | SQLException  ex) {
                
            } 
        });
        
        sbtn.setText("Score");   
        sbtn.setPrefWidth(200);
        sbtn.setDisable(true);
        sbtn.setOnAction((ActionEvent event) -> {
            int rno = SwissPairs.rndsScored + 1;
          //  SwissPairs.usingBM = true;
            Score.allocSS(rno);
            if (Score.Score(rno, false)){
                Draw.makeRanking();
                Draw.printRanking();
                Draw.printSummary(0, rno, rno);
                Draw.printRoundResult(rno);
               // SwissPairs.rndsScored += 1;
                t4.setText("Press 'Make Draw' for Round " + (rno + 1) + " draw.");
                t5.setText("Ranking after Round " + (rno) + ": " + homepath + rankingfilename);
                maintab.setContent(setContent());
            }
        });
        
         
        restartBMbtn.setText("Restart BM");      
        restartBMbtn.setDisable(true);
        restartBMbtn.setOnAction((ActionEvent event) -> {
            try {
            Process process = new ProcessBuilder(SwissPairs.bmpropath + "\\BMPro.exe", "/f:[" + 
                                                     SwissPairs.homepath + dbfile + "]").start();
            }
            catch (IOException ex) {
           // catch (ClassNotFoundException | SQLException  ex) {
                
            } 
        });
        
        
        makedrawbtn.setText("Make Draw");        
        makedrawbtn.setPrefWidth(100); 
        makedrawbtn.setOnAction((ActionEvent event) -> {
           /* try {
                Test.testDB();
            } catch (SQLException ex) {
                Logger.getLogger(SwissPairs.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            Draw.Draw();
            t4.setText("Draw for Round " + (SwissPairs.rndsScored + 1) + ": " + homepath + drawfilename);
            bmbtn.setDisable(false);
        });
        
        
        if(usingBM) rankingbtn.setText("Full Recap");
        else rankingbtn.setText("Print Ranking");
        rankingbtn.setPrefWidth(100); 
        rankingbtn.setOnAction((ActionEvent event) -> {
            Draw.makeRanking();
            Draw.printRanking();
            if(usingBM) Draw.fullRecap();
        });
        
        
        summbtn.setText("Summary");
        summbtn.setPrefWidth(100);        
        summbtn.setOnAction((ActionEvent event) -> {
            try {
                summdialog();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        
       
        editscoresbtn.setText("Edit Scores");
        editscoresbtn.setPrefWidth(100);               
        editscoresbtn.setOnAction((ActionEvent event) -> {          
            editscoresdialog();
            SwissPairs.tabPane.getSelectionModel().select(2);          
        });
        
        
        adjbtn.setText("Adjustment");        
        adjbtn.setPrefWidth(100); 
        adjbtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                   Adjustment.dialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        
        
        removepairbtn.setText("Remove Pair");
        removepairbtn.setPrefWidth(100); 
        removepairbtn.setOnAction((ActionEvent event) -> {          
           removepairdialog();                 
        });
        
        if(rndsScored == 0){
            //makedrawbtn.setDisable(true);
            rankingbtn.setDisable(true);
            summbtn.setDisable(true);
            editscoresbtn.setDisable(true);
            adjbtn.setDisable(true);
            removepairbtn.setDisable(true);
            bmbtn.setDisable(true);
            sbtn.setDisable(true);
        }
        VBox vBox1 = new VBox();
        vBox1.setSpacing(10.0);
        vBox1.setPadding(new Insets(25, 25, 25, 25));
        vBox1.getChildren().addAll(t1,t2,t3);
        vBox1.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid outside;" + 
                      "-fx-border-width: 1;" +
                      "-fx-border-insets: 15;" +                       
                      "-fx-border-color: darkgray;");
        
        HBox hb = new HBox();
        hb.setSpacing(15.0);
        //hb.setPadding(new Insets(25, 25, 25, 25));
        hb.getChildren().addAll(l1, fbdSpinner);
        hb.setAlignment(Pos.CENTER_LEFT);
        
        VBox vBox2 = new VBox();
        vBox2.setSpacing(20.0);
        vBox2.setPadding(new Insets(25, 25, 25, 25));
        vBox2.getChildren().addAll(t6, hb, bmbtn, sbtn, restartBMbtn);
        vBox2.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid outside;" + 
                      "-fx-border-width: 1;" +
                      "-fx-border-insets: 15;" +                       
                      "-fx-border-color: darkgray;");

        VBox vBox3 = new VBox();
        vBox3.setSpacing(30.0);
        vBox3.setPadding(new Insets(25, 25, 25, 25));
        vBox3.getChildren().addAll(makedrawbtn, rankingbtn, summbtn, editscoresbtn, adjbtn,removepairbtn);
        vBox3.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid outside;" + 
                      "-fx-border-width: 1;" +
                      "-fx-border-insets: 15;" +                       
                      "-fx-border-color: darkgray;");
        
        VBox vBox4 = new VBox();
        vBox4.setSpacing(30.0);
        vBox4.setPadding(new Insets(25, 25, 25, 25));
        vBox4.getChildren().addAll(t4,t5);
                
        grid.add(vBox1, 0, 0);
        grid.add(vBox2, 0, 1);
        grid.add(vBox3, 1, 1);
        grid.add(vBox4, 1, 0, 3, 1);
        
        return grid;
    }
    
    public static void editscoresdialog(){
        final Stage dialogStage = new Stage();
                //dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initModality(Modality.APPLICATION_MODAL);

        Label rLabel = new Label("Round No.:");  
        Spinner<Integer> rnoSpinner = new Spinner(1, SwissPairs.rndsScored, 1);        

        Button okBtn = new Button("OK");
        okBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {                        
                SwissPairs.displaytab.setContent(Display.Display(rnoSpinner.getValue()));
                SwissPairs.displaytab.setDisable(false);
                dialogStage.close();
            }
        });

        VBox vBox1 = new VBox();
        vBox1.setSpacing(10.0);
        vBox1.setPadding(new Insets(25, 25, 25, 25));
        vBox1.getChildren().addAll(rLabel, rnoSpinner);
        

        VBox vBox = new VBox();
        vBox.setSpacing(10.0);
        vBox.setPadding(new Insets(25, 25, 25, 25));
        vBox.getChildren().addAll(vBox1, okBtn);

        dialogStage.setScene(new Scene(vBox));
        dialogStage.showAndWait();
    }
    
    public static void removepairdialog(){
        final Stage dialogStage = new Stage();
                //dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initModality(Modality.APPLICATION_MODAL);

        Label pLabel = new Label("Pair No.:");  
        Spinner<Integer> pnoSpinner = new Spinner(1, SwissPairs.pairs, 1);        

        Button okBtn = new Button("OK");
        okBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {   
                if(SwissPairs.withdrawn[pnoSpinner.getValue()] == false){ //can't remove the same pair twice
                    if((SwissPairs.pairs - SwissPairs.withdrawnpairs) % 2 == 0 ) SwissPairs.tables -= 1;
                    SwissPairs.withdrawn[pnoSpinner.getValue()] = true;
                    SwissPairs.withdrawnpairs += 1;   
                }
                dialogStage.close();
            }
        });

        VBox vBox1 = new VBox();
        vBox1.setSpacing(10.0);
        vBox1.setPadding(new Insets(25, 25, 25, 25));
        vBox1.getChildren().addAll(pLabel, pnoSpinner);
        

        VBox vBox = new VBox();
        vBox.setSpacing(10.0);
        vBox.setPadding(new Insets(25, 25, 25, 25));
        vBox.getChildren().addAll(vBox1, okBtn);

        dialogStage.setScene(new Scene(vBox));
        dialogStage.showAndWait();
    }
    
    public static void summdialog(){
         final Stage dialogStage = new Stage();
                //dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initModality(Modality.APPLICATION_MODAL);

        Label pLabel = new Label("Pair No.:");  
        Spinner<Integer> pnoSpinner = new Spinner(0, SwissPairs.pairs, 0); 
        
        Label sLabel = new Label("Starting Round:");  
        Spinner<Integer> sSpinner = new Spinner(0, SwissPairs.rndsScored, 1); 
        
        Label eLabel = new Label("Ending Round:");  
        Spinner<Integer> eSpinner = new Spinner(sSpinner.getValue(), SwissPairs.rndsScored, sSpinner.getValue()); 

        Button okBtn = new Button("OK");
        okBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {   
                Draw.printSummary(pnoSpinner.getValue(), sSpinner.getValue(),eSpinner.getValue());
                dialogStage.close();
            }
        });

        VBox vBox1 = new VBox();
        vBox1.setSpacing(10.0);
        vBox1.setPadding(new Insets(25, 25, 25, 25));
        vBox1.getChildren().addAll(pLabel, pnoSpinner, sLabel, sSpinner, eLabel, eSpinner);
        

        VBox vBox = new VBox();
        vBox.setSpacing(10.0);
        vBox.setPadding(new Insets(25, 25, 25, 25));
        vBox.getChildren().addAll(vBox1, okBtn);

        dialogStage.setScene(new Scene(vBox));
        dialogStage.showAndWait();
    }
}
