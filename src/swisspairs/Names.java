/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Kaustubh Bendre
 */
public class Names {
    public static ScrollPane Names () {
        ScrollPane scrollPane = new ScrollPane();
                
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        Text scenetitle = new Text("Pair Names:");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);        
           
        int i, j, k;
        String str1;
        
        k = SwissPairs.pairs;
        
        TextField[][] fields = new TextField[k+1][2];
        
        for (i=1; i <= k; i++){
            str1 = "Pair " + i;
                        
            Label label = new Label(str1);
            grid.add(label, 0, i);

            TextField p1 = new TextField();
            grid.add(p1, 1, i);
            fields[i][0] = p1;
            p1.setText(SwissPairs.pairnames[i][0]);
            
            TextField p2 = new TextField();
            grid.add(p2, 2, i);
            fields[i][1] = p2;
            p2.setText(SwissPairs.pairnames[i][1]);
     
            Text pen = new Text(String.valueOf(SwissPairs.penalties[i]));
            grid.add(pen,4,i);
        }
        
        Button btn = new Button();
        btn.setText("Save");     
        btn.setMinSize(70,70);
        grid.add(btn, 6, 1, 2, 2);
        btn.setOnAction((ActionEvent event) -> {
            savenames(fields);           
        });
        
        Button lbtn = new Button();
        lbtn.setText("Load");     
        lbtn.setMinSize(70,70);
        grid.add(lbtn, 6, 4, 2, 2);
        lbtn.setOnAction((ActionEvent event) -> {
            String[][] lfields = loadnames();
            //loadnames(fields); 
         //   System.out.println("bbb");
            for(int ii = 1; ii <= k; ii++){
                fields[ii][0].setText(lfields[ii][0]);
                fields[ii][1].setText(lfields[ii][1]);
            }
        });
        
        scrollPane.setContent(grid);
        return scrollPane;
    }
    
    public static void savenames(TextField[][] fields){
        try {
            File namesfile = new File(SwissPairs.homepath + "names.csv");
            if(namesfile.exists()){
                File bufile = new File(SwissPairs.homepath + "bu_names.csv");
                namesfile.renameTo(bufile);
                namesfile.delete();
            }
            String str = "";
            for (int i1 = 1; i1 <= SwissPairs.pairs; i1++) {
                SwissPairs.pairnames[i1][0] = fields[i1][0].getText();
                SwissPairs.pairnames[i1][1] = fields[i1][1].getText();     
                str += i1 + "," + fields[i1][0].getText() + "," + fields[i1][1].getText() + "\n";
            }
            FileWriter fr = new FileWriter(namesfile);
            fr.write(str);
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static String[][] loadnames(){
        String[][] fields = new String[SwissPairs.pairs +1][2];
         try {
            
            File namesfile = new File(SwissPairs.homepath + "names.csv");
            if(namesfile.exists()){
                FileReader frd = new FileReader(namesfile);
                BufferedReader brd = new BufferedReader(frd);
             //   System.out.println("ccc");
                String str;    
                String[] sa;
                int pno = 0;
                while ((str=brd.readLine())!=null){
                    sa = str.split(",");
             //       System.out.println(str);
                    if(sa.length > 2){
                        if(sa[0].matches("[+-]?\\d+")) pno = Integer.parseInt(sa[0]);
                        if(pno > 0 && pno <= SwissPairs.pairs){
                            SwissPairs.pairnames[pno][0] = sa[1].trim();
                            SwissPairs.pairnames[pno][1] = sa[2].trim();
                            fields[pno][0] = sa[1].trim();
                            fields[pno][1] = sa[2].trim();
                        }
               //         System.out.println(sa[1]);
                    }
                }
                brd.close();
                frd.close();
            }    
         
            
        } catch (IOException ex) {
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }
         return fields;
    }
    
}