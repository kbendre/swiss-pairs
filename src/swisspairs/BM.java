/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import java.io.File;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Kaustubh Bendre
 */
public class BM {
  
    public static void initiateDB() throws ClassNotFoundException, SQLException{
        PreparedStatement ps = null;
        String query;
        ResultSet rs = null;
        Connection conn;
        Boolean restart = false;
        File dbfile = new File(SwissPairs.homepath + SwissPairs.dbfile);     
       
        //System.out.println(dbfile);
        if (SwissPairs.rndsScored == 0 && dbfile.isFile()){
            Alert alert = new Alert(AlertType.CONFIRMATION, "Restart Old Event?", ButtonType.YES, ButtonType.NO);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                conn  =DriverManager.getConnection("jdbc:ucanaccess://" + dbfile + ";immediatelyReleaseResources=true");
                restart = true;
            }
            else {                
                int i = 1;                
                File bufile = new File(SwissPairs.homepath + "backup_" + i + "_" + SwissPairs.dbfile);
                while(bufile.exists()){
                    i++;
                    bufile = new File(SwissPairs.homepath + "backup_" + i + "_" + SwissPairs.dbfile);
                }
                dbfile.renameTo(bufile);
                dbfile.delete();
                conn  =DriverManager.getConnection("jdbc:ucanaccess://" + dbfile + ";newdatabaseversion=V2000;immediatelyReleaseResources=true");
            }
        }
        else {
            conn  =DriverManager.getConnection("jdbc:ucanaccess://" + dbfile + ";newdatabaseversion=V2000;immediatelyReleaseResources=true");
        }
       
        try{
            int tbls = SwissPairs.tables;
            int rno = SwissPairs.rndsScored + 1;
            int cID = 1;
                      
            //ps = conn.prepareStatement("");
            
            
            if(rno == 1 && restart == false){
                fillDB(conn);

                String ComputerName = InetAddress.getLocalHost().getHostName();
                query = "Insert into Clients (ID, Computer) Values (1,'" + ComputerName + "')";
                ps = conn.prepareStatement(query);
                ps.execute();

                query = "Insert into Session (ID, Name) Values (1,'" + SwissPairs.nameOfEvent + "')";
                ps = conn.prepareStatement(query);
                ps.execute();                
             

                query = "Insert into Section (ID, Letter, Tables, EWMoveBeforePlay, Session) "
                        + "Values ('1','A','" + tbls + "','0','1')";
                ps = conn.prepareStatement(query);
                ps.execute();                
                
                cID = 1;
                query = "Select ID from Clients";
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                while(rs.next()) {
                cID = rs.getInt("ID");
                }
                
                query = "Insert into Tables (Section, [Table], ComputerID, UpdateFromRound) Values ";
                for (int i = 1; i < tbls; i++) query += "('1','" + i + "','" + cID + "','" + "257" + "'),";    
                query += "('1','" + tbls + "','" + cID  + "','" + "257" + "')";       
                ps = conn.prepareStatement(query);
                ps.execute();  
                
                query = "Insert into Settings (Section, "
                    + "ShowOwnResult, "
                    + "BM2ShowPlayerNames, "
                    + "BM2ScoreRecap, "
                    + "BM2AutoShowScoreRecap) "
                    + "Values ('1', 'true', '1', 'true', 'true')";
                   // + "Values ('1', '1', '1', '1', '1')";
                ps = conn.prepareStatement(query);
                ps.execute(); 
            }
            
            if(restart == false){
             //   Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            //    String myDB = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=" + dbfile;
            //    conn.close();
             //   conn = DriverManager.getConnection(myDB, "", "");
                uploadRoundData(conn, rno, cID);
                Score.editScoreFile(rno);
            }
            
            
        }
        catch (Exception e) {
			e.printStackTrace();
	}
        
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /*ignored*/ }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /*ignored*/ }
            }
            if (conn != null) {
                try {
                  //  System.out.println("Closing conn in initiateDB");
                    conn.close();
                 //   System.out.println("Closed conn in initiateDB");
                } catch (SQLException e) { /*ignored*/ }
            }
        }
    }        
    
    public static void uploadRoundData(Connection conn, int rno, int cID) throws SQLException{        
        int tbls = SwissPairs.tables;        
        String query;
        PreparedStatement ps;
       // ResultSet rs;
        try{       
            int ufr;
            if (SwissPairs.lastround) ufr = rno;  
            else ufr = 256 + rno;// 256? see BMdevguide page 36.  
             
          /*   query = "insert into RoundData (Section, [Table], Round, NSPair, EWPair, LowBoard, HighBoard) values ";
           for(int i=1; i<tbls; i++){
                query += "('1','" 
                        + i + "','" + (SwissPairs.rndsScored + 1) + "','" + SwissPairs.newdraw[(i*2)-1] + "','" 
                        + SwissPairs.newdraw[(i*2)] + "','" + SwissPairs.firstbd[SwissPairs.rndsScored + 1] + 
                        "','" + (SwissPairs.firstbd[SwissPairs.rndsScored + 1] + SwissPairs.bdsperrnd -1) + "'), "; //note the comma at the end
            }  
            query += "('1','" 
                    + tbls + "','" + (SwissPairs.rndsScored + 1) + "','" + SwissPairs.newdraw[(tbls*2)-1] + "','" 
                        + SwissPairs.newdraw[(tbls*2)] + "','" + SwissPairs.firstbd[SwissPairs.rndsScored + 1] + 
                        "','" + (SwissPairs.firstbd[SwissPairs.rndsScored + 1] + SwissPairs.bdsperrnd -1) + "') "; //no comma at the end
                       
 
            ps = conn.prepareStatement(query);            
            ps.execute();      
            
            query = "Insert into Tables (Section, [Table], ComputerID, UpdateFromRound) Values ";
            for (int i = 1; i < tbls; i++) query += "('1','" + i + "','" + cID + "','" + ufr + "'),";    
            query += "('1','" + tbls + "','" + cID  + "','" + ufr + "')";       
            ps = conn.prepareStatement(query);
            ps.execute();  
          
            query = "delete from Tables";
            ps = conn.prepareStatement(query);
            ps.execute();
            
            
            int nsno, ewno;
            String n[], n1, n2;
            query = "Insert into PlayerNumbers (Section, [Table], Direction, [Name], Updated) Values ";
            for(int i = 1; i < tbls; i++){
                nsno = SwissPairs.newdraw[(i*2)-1];
                ewno = SwissPairs.newdraw[i*2];
                n1 = SwissPairs.pairnames[nsno][0];
                if(n1 != null) {
                    n = n1.split(" ");
                    n1 = n[n.length - 1];
                    if(n1.length() > 17) n1 = n1.substring(0, 17);
                }
                n2 = SwissPairs.pairnames[nsno][1];
                if(n2 != null) {
                    n = n2.split(" ");
                    n2 = n[n.length - 1];
                    if(n2.length() > 17) n2 = n2.substring(0, 17);
                }
                query+= "('1','" + i + "','N','" +  n1 + "','true'),";
                query+= "('1','" + i + "','S','" +  n2 + "','true'),";
                n1 = SwissPairs.pairnames[ewno][0];
                if(n1 != null) {
                    n = n1.split(" ");
                    n1 = n[n.length - 1];
                    if(n1.length() > 17) n1 = n1.substring(0, 17);
                }
                n2 = SwissPairs.pairnames[ewno][1];
                if(n2 != null) {
                    n = n2.split(" ");
                    n2 = n[n.length - 1];
                    if(n2.length() > 17) n2 = n2.substring(0, 17);
                }
                query+= "('1','" + i + "','E','" +  n1 + "','true'),";
                query+= "('1','" + i + "','W','" +  n2 + "','true'),";
            }
            nsno = SwissPairs.newdraw[(tbls*2)-1];
            ewno = SwissPairs.newdraw[tbls*2];
            n1 = SwissPairs.pairnames[nsno][0];
            if(n1 != null) {
                n = n1.split(" ");
                n1 = n[n.length - 1];
                if(n1.length() > 17) n1 = n1.substring(0, 17);
            }
            n2 = SwissPairs.pairnames[nsno][1];
            if(n2 != null) {
                n = n2.split(" ");
                n2 = n[n.length - 1];
                if(n2.length() > 17) n2 = n2.substring(0, 17);
            }
            query+= "('1','" + tbls + "','N','" +  n1 + "','true'),";
            query+= "('1','" + tbls + "','S','" +  n2 + "','true'),";
            n1 = SwissPairs.pairnames[ewno][0];
            if(n1 != null) {
                n = n1.split(" ");
                n1 = n[n.length - 1];
                if(n1.length() > 17) n1 = n1.substring(0, 17);
            }
            n2 = SwissPairs.pairnames[ewno][1];
            if(n2 != null) {
                n = n2.split(" ");
                n2 = n[n.length - 1];
                if(n2.length() > 17) n2 = n2.substring(0, 17);
            }
            query+= "('1','" + tbls + "','E','" +  n1 + "','true'),";
            query+= "('1','" + tbls + "','W','" +  n2 + "','true')";
            ps = conn.prepareStatement(query);
            ps.execute();  
            */
            query = "insert into RoundData (Section, [Table], Round, NSPair, EWPair, LowBoard, HighBoard) values (?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            for(int i=1; i<=tbls; i++){
               ps.setString(1,"1");
               ps.setString(2,Integer.toString(i));
               ps.setString(3,Integer.toString(SwissPairs.rndsScored + 1));
               ps.setString(4,Integer.toString(SwissPairs.newdraw[(i*2)-1]));
               ps.setString(5,Integer.toString(SwissPairs.newdraw[(i*2)]));
               ps.setString(6,Integer.toString(SwissPairs.firstbd[SwissPairs.rndsScored + 1]));               
               ps.setString(7,Integer.toString((SwissPairs.firstbd[SwissPairs.rndsScored + 1] + SwissPairs.bdsperrnd -1)));
               ps.addBatch();
            }
           
            ps.executeBatch();
           // System.out.println("OK insert into RoundData");
             
            
            query = "delete from Tables";
            ps = conn.prepareStatement(query);
            ps.execute();
          //  System.out.println("OK delete from Tables");
           
            query = "Insert into Tables (Section, [Table], ComputerID, UpdateFromRound) Values (?,?,?,?)";
            ps = conn.prepareStatement(query);
            for (int i = 1; i <= tbls; i++){
                ps.setString(1,"1"); 
                ps.setString(2,Integer.toString(i));
                ps.setString(3,Integer.toString(cID));
                ps.setString(4,Integer.toString(ufr));
                ps.addBatch();
            }
            ps.executeBatch();
          //  System.out.println("OK Insert into Tables");
            
            query = "delete from PlayerNumbers";
            ps = conn.prepareStatement(query);
            ps.execute();
         //   System.out.println("OK delete from PlayerNumbers");
            
            int nsno, ewno;
            String n[], n1, n2, n3, n4;
            String[] dir = {"", "N", "S", "E", "W"};
            String[] names = new String[5];
            query = "Insert into PlayerNumbers (Section, [Table], Direction, [Name], Updated) Values (?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            for (int i = 1; i <= tbls; i++){
                nsno = SwissPairs.newdraw[(i*2)-1];
                ewno = SwissPairs.newdraw[i*2];
                n1 = SwissPairs.pairnames[nsno][0];
                if(n1 != null) {
                    n = n1.split(" ");
                    n1 = n[n.length - 1];
                    if(n1.length() > 17) n1 = n1.substring(0, 17);
                }
                n2 = SwissPairs.pairnames[nsno][1];
                if(n2 != null) {
                    n = n2.split(" ");
                    n2 = n[n.length - 1];
                    if(n2.length() > 17) n2 = n2.substring(0, 17);
                }
                n3 = SwissPairs.pairnames[ewno][0];
                if(n3 != null) {
                    n = n3.split(" ");
                    n3 = n[n.length - 1];
                    if(n3.length() > 17) n3 = n3.substring(0, 17);
                }
                n4 = SwissPairs.pairnames[ewno][1];
                if(n4 != null) {
                    n = n4.split(" ");
                    n4 = n[n.length - 1];
                    if(n4.length() > 17) n4 = n4.substring(0, 17);
                }
                names[1] = n1;
                names[2] = n2;
                names[3] = n3;
                names[4] = n4;
                for(int j = 1; j <= 4; j++){
                    ps.setString(1,"1"); 
                    ps.setString(2,Integer.toString(i));
                    ps.setString(3,dir[j]);
                    ps.setString(4,names[j]);
                    ps.setString(5,"true");
                    ps.addBatch();
                }
            }
            ps.executeBatch();
         //   System.out.println("OK Insert into names");
            
        }
        catch (SQLException e) {
            System.out.println("Error in uploadRoundData!");
        }
    }
    
    public static void uploadSettings(Connection conn) throws SQLException {  
        try{
            
        }
        catch (Exception e) {
			e.printStackTrace();
	}
    }

    public static void fillDB(Connection conn) throws SQLException {
       // ResultSet rs;
        PreparedStatement ps = null;
        String query;
        
        try{          
        Statement s = conn.createStatement();
        s.executeUpdate("CREATE TABLE Clients (ID counter, Computer Text(255))");

        s.executeUpdate("CREATE TABLE Session (ID int, Name Text(40), [Date] Datetime, [Time] Datetime, [GUID] Text(80), Status Int DEFAULT '0', ShowInApp yesno DEFAULT 'no', PairsMoveAcrossField yesno DEFAULT 'no', EWReturnHome yesno DEFAULT 'no')");
        
        s.executeUpdate("CREATE TABLE Section (ID int, Letter text(3), Tables int, MissingPair int DEFAULT '0', EWMoveBeforePlay int DEFAULT '0', Session int, ScoringType int DEFAULT '1', Winners int DEFAULT '0')");
        
        s.executeUpdate("CREATE TABLE Tables (Section int, [Table] int, ComputerID int DEFAULT '0', Status int DEFAULT '0', LogOnOff int DEFAULT '2', UpdateFromRound int DEFAULT '0', CurrentRound int DEFAULT '0', CurrentBoard int DEFAULT '0', [Group] int DEFAULT '0')");
        
        s.executeUpdate("CREATE TABLE RoundData (Section int, [Table] int, Round int, NSPair int, EWPair int, LowBoard int, HighBoard int, CustomBoards text(255))");
        
        s.executeUpdate("CREATE TABLE ReceivedData (ID counter, Section int, [Table] int, Round int, Board int, PairNS int, PairEW int, Declarer int, [NS/EW] text(2), Contract text(10), Result text(10), LeadCard text(10), Remarks text(255), DateLog datetime, TimeLog datetime, Processed yesno NOT NULL DEFAULT 'no', ExternalUpdate yesno NOT NULL DEFAULT 'no', Processed1 yesno NOT NULL DEFAULT 'no', Processed2 yesno NOT NULL DEFAULT 'no', Processed3 yesno NOT NULL DEFAULT 'no', Processed4 yesno NOT NULL DEFAULT 'no', Erased yesno NOT NULL DEFAULT 'no', SuspiciousContract yesno NOT NULL DEFAULT '0')");
        
        s.executeUpdate("CREATE TABLE IntermediateData (ID counter, Section int, [Table] int, Round int, Board int, PairNS int, PairEW int, Declarer int, [NS/EW] text(2), Contract text(10), Result text(10), LeadCard text(10), Remarks text(255), DateLog datetime, TimeLog datetime, Processed yesno NOT NULL DEFAULT 'no', ExternalUpdate yesno NOT NULL DEFAULT 'no', Processed1 yesno NOT NULL DEFAULT 'no', Processed2 yesno NOT NULL DEFAULT 'no', Processed3 yesno NOT NULL DEFAULT 'no', Processed4 yesno NOT NULL DEFAULT 'no', Erased yesno NOT NULL DEFAULT 'no', SuspiciousContract yesno NOT NULL DEFAULT '0')");
        
        s.executeUpdate("CREATE TABLE PlayerNumbers (Section int, [Table] int, Direction text(2), Number text(16), Name text(18), Round int DEFAULT '0', Updated yesno NOT NULL DEFAULT 'no', TimeLog datetime, Processed yesno NOT NULL DEFAULT 'no')");
        
        s.executeUpdate("CREATE TABLE PlayerNames (ID int, " + 
                        "Name text(18), strID text(18))");
        
        s.executeUpdate("CREATE TABLE Settings (" +
                        "Section int," +
                        "ShowResults yesno DEFAULT 'yes'," +
                        "ShowOwnResult yesno DEFAULT 'yes'," +
                        "RepeatResults yesno DEFAULT 'no'," +
                        "MaximumResults int DEFAULT '0'," +
                        "ShowPercentage yesno DEFAULT 'yes'," +
                        "GroupSections yesno DEFAULT 'no'," +
                        "ScorePoints int DEFAULT '1'," +
                        "EnterResultsMethod int DEFAULT '0'," +
                        "ShowPairNumbers yesno DEFAULT 'yes'," +
                        "IntermediateResults yesno DEFAULT 'no'," +
                        "AutopoweroffTime int DEFAULT '20'," +
                        "VerificationTime int DEFAULT '2'," +
                        "ShowContract int DEFAULT '1'," +
                        "LeadCard yesno DEFAULT 'no'," +
                        "MemberNumbers yesno DEFAULT 'no'," +
                        "MemberNumbersNoBlankEntry yesno DEFAULT 'no'," +
                        "BoardOrderVerification yesno DEFAULT 'yes'," +
                        "HandRecordValidation yesno DEFAULT 'yes'," +
                        "AutoShutDownBPC yesno DEFAULT 'no'," +
                        "BM2PINcode text(4) DEFAULT '0000'," +
                        "BM2ConfirmNP yesno DEFAULT 'no'," +
                        "BM2TDCall yesno DEFAULT 'no'," +
                        "BM2RemainingBoards yesno DEFAULT 'yes'," +
                        "BM2NextSeatings yesno DEFAULT 'yes'," +
                        "BM2ScoreRecap yesno DEFAULT 'no'," +
                        "BM2AutoShowScoreRecap yesno DEFAULT 'no'," +
                        "BM2ScoreCorrection yesno DEFAULT 'no'," +
                        "BM2AutoBoardNumber yesno DEFAULT 'yes'," +
                        "BM2FirstBoardManually yesno DEFAULT 'no'," +
                        "BM2ValidateLeadCard yesno DEFAULT 'no'," +
                        "BM2ResultsOverview int DEFAULT '0'," +
                        "BM2ShowPlayerNames int DEFAULT '0'," +
                        "BM2Ranking int DEFAULT '0'," +
                        "BM2GameSummary yesno DEFAULT 'no'," +
                        "BM2SummaryPoints int DEFAULT '0'," +
                        "BM2PairNumberEntry int DEFAULT '0'," +
                        "BM2ResetFunctionKey yesno DEFAULT 'yes'," +
                        "BM2RecordBidding yesno DEFAULT 'no'," +
                        "BM2RecordPlay yesno DEFAULT 'no'," + 
                        "BM2ValidateRecording yesno DEFAULT 'no'," +
                        "BM2ShowHands yesno DEFAULT 'no'," +
                        "BM2NumberValidation int DEFAULT '0'," +
                        "BM2NumberEntryEachRound yesno DEFAULT 'no'," +
                        "BM2NumberEntryPreloadValues yesno DEFAULT 'no'," +
                        "BM2NameSource int DEFAULT '0'," +
                        "BM2ViewHandRecord yesno DEFAULT 'no'," +
                        "BM2EnterHandRecord yesno DEFAULT 'no'," +
                        "BM2EnterHandRecordWhen int DEFAULT '0'," +
                        "BM2TextBasedNumber yesno DEFAULT 'no')");
        
        s.executeUpdate("CREATE TABLE BiddingData (ID counter, Section int, [Table] int, Round int, Board int, Counter int, Direction text(2), Bid text(10), DateLog [date], TimeLog [date], Erased yesno DEFAULT 'no')");
        
        s.executeUpdate("CREATE TABLE PlayData (ID counter, Section int, [Table] int, Round int, Board int, Counter int, Direction text(2), Card text(10), DateLog [Date], TimeLog [date], Erased yesno DEFAULT 'no')");
        
        s.executeUpdate("CREATE TABLE HandRecord (Section int, Board int, NorthSpades text(13), NorthHearts text(13), NorthDiamonds text(13), NorthClubs text(13), EastSpades text(13), EastHearts text(13), EastDiamonds text(13), EastClubs text(13), SouthSpades text(13), SouthHearts text(13), SouthDiamonds text(13), SouthClubs text(13), WestSpades text(13), WestHearts text(13), WestDiamonds text(13), WestClubs text(13))");
        
        s.executeUpdate("CREATE TABLE HandEvaluation (Section int, Board int, NorthSpades int, NorthHearts int, NorthDiamonds int, NorthClubs int, EastSpades int, EastHearts int, EastDiamonds int, EastClubs int, SouthSpades int, SouthHearts int, SouthDiamonds int, SouthClubs int, WestSpades int, WestHearts int, WestDiamonds int, WestClubs int, NorthHcp int, EastHcp int, SouthHcp int, WestHcp int)");
        
        }
        catch (Exception e) {
			e.printStackTrace();
	}
        
    }
   
}
