/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Kaustubh Bendre
 */
public class Draw {
    public static void Draw () {        
        makeRanking();
        SwissPairs.newdraw = makeDraw(SwissPairs.leaders, SwissPairs.pairs - SwissPairs.withdrawnpairs);
        if(SwissPairs.newdraw[0] == 0) {
            errorDialog("Exiting Swiss Pairs...");
        }
        else {
            printRanking();
            printDraw();
            for(int i = 1, ns, ew; i <= SwissPairs.tables; i++){
                ns = SwissPairs.newdraw[2*i - 1];
                ew = SwissPairs.newdraw[2*i];
                SwissPairs.drawhistory[ns][SwissPairs.rndsScored+1] = i;
                SwissPairs.drawhistory[ew][SwissPairs.rndsScored+1] = i;
            }
            int byepairno = SwissPairs.newdraw[SwissPairs.pairs - SwissPairs.withdrawnpairs];
            if(SwissPairs.pairs % 2 == 1) SwissPairs.drawhistory[byepairno][SwissPairs.rndsScored+1] = 0;
        }
    }
    
    public static void makeRanking(){
        SwissPairs.leaders = new int[SwissPairs.pairs - SwissPairs.withdrawnpairs + 1];
        int i, j, k, indexofmax;
        double max;
        int pairs = SwissPairs.pairs;
        double[] temp = new double[pairs + 1];
        //int[] newdraw = new int[pairs+1];
        
        //sort by vpscores and update SwissPairs.leaders        
        for(i=1; i <= pairs; i++){
            temp[i] = SwissPairs.crosstable[i][0];
        }
        
        k = 1;
        max = 0;
        indexofmax = 1;
        for(i=1; i<= pairs; i++){
            for(j = pairs; j > 0 ; j--){
                if(temp[j] >= max){
                    max = temp[j];
                    indexofmax = j;                    
                }
            }
            temp[indexofmax] = -1;
           
            if(SwissPairs.withdrawn[indexofmax] == false){
                SwissPairs.leaders[k] = indexofmax;
                k++;
            }
            max = 0;
        }
        ///////////////////////////    

    }
    
    public static int[] makeDraw(int[] inputarray, int pairs){
        int[] returnarray;
        
    /*    if (pairs == 1){
            if(SwissPairs.crosstable[inputarray[1]][SwissPairs.MAX_PAIRS-1] == 0){
                returnarray = new int[2];
                returnarray[0] = 1; //success
                returnarray[1] = inputarray[1];
                return returnarray;
            }
            else{
                returnarray = new int[1];
                returnarray[0] = 0;//failure
                return returnarray;
            }
        }*/
     
          
        if (pairs == 2){
           if(SwissPairs.crosstable[inputarray[1]][inputarray[2]] == 0){
                returnarray = new int[3];
                returnarray[0] = 1; //success
                returnarray[1] = inputarray[1];
                returnarray[2] = inputarray[2];
                return returnarray;
            }
            else{
                returnarray = new int[1];
                returnarray[0] = 0;//failure
                return returnarray;
            } 
        }
        int[] fwdarray, intermediatearray;
        int p2;
        
        //Ensure that the pair with the least score gets a bye (if they haven't got a bye before)
        if (pairs%2 == 1){
            int i = 0;
            p2 = inputarray[pairs - i];
            while (SwissPairs.crosstable[p2][SwissPairs.MAX_PAIRS-1] != 0 && i <= pairs){
                i++;
                p2 = inputarray[pairs - i];                             
            }
            fwdarray = new int[pairs];
            for(int j=1, k=1; j <= pairs; j++){
                 if(inputarray[j] != p2) {
                     fwdarray[k] = inputarray[j];
                     k++;
                 }
            }
            intermediatearray = makeDraw(fwdarray,pairs-1);
            if(intermediatearray[0]==0) return intermediatearray;//failure
            returnarray = new int[pairs+1];
            returnarray[0] = 1;            
            //System.arraycopy(intermediatearray, 1, returnarray, 1, pairs);
            for(i = 1; i < pairs; i++) returnarray[i] = intermediatearray[i];
            returnarray[pairs] = p2;
            return returnarray;//success
        }
        
        
        
        for(int i=2, p1 = inputarray[1] ; i<=pairs; i++){
            p2 = inputarray[i];
            if(SwissPairs.crosstable[p1][p2] == 0 && SwissPairs.crosstable[p2][p1] == 0){
                fwdarray = new int[pairs-1];
                //fwdarray[1] = p1;
                for(int j=2, k=1; j <= pairs; j++){
                    if(j!=i) {
                        fwdarray[k] = inputarray[j];
                        k++;
                    }
                }
                intermediatearray = makeDraw(fwdarray,pairs-2);
                //if(intermediatearray[0]==0) return intermediatearray;//failure
                if(intermediatearray[0]==0) continue;
                
                returnarray = new int[pairs+1];
                returnarray[0] = 1;
                returnarray[1] = p1;
                returnarray[2] = p2;
                System.arraycopy(intermediatearray, 1, returnarray, 3, pairs-1 - 1);
                return returnarray;//success
            }
        }
        //if this doesn't get a draw, try shifting p1 to 2 -->
        for(int i=3, p1 = inputarray[2] ; i<=pairs; i++){
            p2 = inputarray[i];
            if(SwissPairs.crosstable[p1][p2] == 0 && SwissPairs.crosstable[p2][p1] == 0){
                fwdarray = new int[pairs-1];
                fwdarray[1] = inputarray[1];
                for(int j=3, k=2; j <= pairs; j++){
                    if(j!=i) {
                        fwdarray[k] = inputarray[j];
                        k++;
                    }
                }
                intermediatearray = makeDraw(fwdarray,pairs-2);
                //if(intermediatearray[0]==0) return intermediatearray;//failure
                if(intermediatearray[0]==0) continue;
                
                returnarray = new int[pairs+1];
                returnarray[0] = 1;
                returnarray[1] = p1;
                returnarray[2] = p2;
                System.arraycopy(intermediatearray, 1, returnarray, 3, pairs-1 - 1);
                return returnarray;//success
            }
        }
        returnarray = new int[1];
        returnarray[0] = 0;//failure
        return returnarray;
    }
    
    public static void printRanking(){
        String td = "</td><td>";
        String row = "<tr><td>";
        String rowend = "</td></tr>";
        SwissPairs.rankingfilename = "Ranking_after_R" + SwissPairs.rndsScored + ".htm"; 
        File outputfile;
        outputfile = new File(SwissPairs.homepath + SwissPairs.rankingfilename );
        String str;
        
         str = "<html><style>"
                 + "* {box-sizing: border-box;}"
                 + "table {font-family: arial, sans-serif;border-collapse: collapse;width: 75%;}"
                 + "td, th {border: 1px solid #dddddd;text-align: center; width: auto; padding: 8px;}"
                 + "div { margin: 20px;}"
                 + ".column {float: left; width: 40%; padding: 30px;}"
                 + ".row:after {content: '';display: table;  clear: both;  padding: 30px;}"
                 + "</style>"
                 + "<body><div>\n\n";

        str += "<h3>" + SwissPairs.nameOfEvent + "</h3> \n\n";
        str += "<h3>Leaders after Round " + SwissPairs.rndsScored + "</h3></div>\n\n";
        
        str += "<p><table style=\"width:500px\"><th>Rank</th><th>No.</th><th>Name</th><th>VP</th>"; 
        int rank;
        String vpstr;
        for(int i = 1; i<= (SwissPairs.pairs - SwissPairs.withdrawnpairs); i++){
            rank = SwissPairs.leaders[i];
            //vpstr = String.valueOf(SwissPairs.crosstable[rank][0]);
            vpstr = String.format("%.2f", SwissPairs.crosstable[rank][0]);
            str += row + i + td + rank + td + SwissPairs.pairnames[rank][0] + 
                    " - " + SwissPairs.pairnames[rank][1] + td + vpstr + rowend;
        }
        
        str += "</table></p><br/><br/></div></body></html>";
        try {
            FileWriter fr = new FileWriter(outputfile);
            fr.write(str);
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(Draw.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }  
    
    public static void printDraw(){
        String td = "</td><td>";
        String row = "<tr><td>";
        String rowend = "</td></tr>";
        SwissPairs.drawfilename = "Draw_for_R" + (SwissPairs.rndsScored + 1) + ".htm"; 
        File outputfile;
        outputfile = new File(SwissPairs.homepath + SwissPairs.drawfilename );
        String str;
        
         str = "<html><style>"
                 + "* {box-sizing: border-box;}"
                 + "table {font-family: arial, sans-serif;border-collapse: collapse;width: 75%;}"
                 + "td, th {border: 1px solid #dddddd;text-align: center; width: auto; padding: 8px;}"
                 + "div { margin: 20px;}"
                 + ".column {float: left; width: 40%; padding: 30px;}"
                 + ".row:after {content: '';display: table;  clear: both;  padding: 30px;}"
                 + "</style>"
                 + "<body><div>\n\n";

        //str += "<h3>" + SwissPairs.nameOfEvent + "</h3> \n\n";
        str += "<h3>Draw for Round " + (SwissPairs.rndsScored + 1) + "</h3></div>\n\n";
        
        str += "<p><table><th>VP</th><th>No.</th><th>Name</th><th>Table</th><th>Name</th><th>No.</th><th>VP</th>"; 
        int nsno,ewno;
        String vpstr1, vpstr2, n1, n2, n3, n4;
        String[] n;
        for(int i = 1; i<= SwissPairs.tables; i++){
            nsno = SwissPairs.newdraw[i*2-1];
            ewno = SwissPairs.newdraw[i*2];
            //vpstr = String.valueOf(SwissPairs.crosstable[rank][0]);
            vpstr1 = String.format("%.2f", SwissPairs.crosstable[nsno][0]);
            vpstr2 = String.format("%.2f", SwissPairs.crosstable[ewno][0]);
            n1 = SwissPairs.pairnames[nsno][0];
            if(n1 != null) {
                n = n1.split(" ");
                n1 = n[n.length - 1];
            }
            
            n2 = SwissPairs.pairnames[nsno][1];
            if(n2 != null) {
                n = n2.split(" ");
                n2 = n[n.length - 1];
            }
            n3 = SwissPairs.pairnames[ewno][0];
            if(n3 != null) {
                n = n3.split(" ");
                n3 = n[n.length - 1];
            }
            n4 = SwissPairs.pairnames[ewno][1];
            if(n4 != null) {
                n = n4.split(" ");
                n4 = n[n.length - 1];
            }
            
            
            
            str += row + vpstr1 + td + nsno + td + n1 + 
                    " - " + n2 + td + i + td +
                    n3 + " - " + n4 + 
                    td + ewno + td + vpstr2 + rowend;
        }
        if((SwissPairs.pairs - SwissPairs.withdrawnpairs) % 2 == 1){
            nsno = SwissPairs.newdraw[SwissPairs.pairs - SwissPairs.withdrawnpairs];
            vpstr1 = String.format("%.2f", SwissPairs.crosstable[nsno][0]);
            n1 = SwissPairs.pairnames[nsno][0];
            if(n1 != null) {
                n = n1.split(" ");
                n1 = n[n.length - 1];
            }
            n2 = SwissPairs.pairnames[nsno][1];
            if(n2 != null) {
                n = n2.split(" ");
                n2 = n[n.length - 1];
            }
            str += row + vpstr1 + td + nsno + td + n1 + 
                    " - " + n2 + td + (SwissPairs.tables + 1) +
                    td + "BYE" + td + "--" + td + "--" + rowend;
        }
        
        str += "</table></p><br/><br/></div></body></html>";
        try {
            FileWriter fr = new FileWriter(outputfile);
            fr.write(str);
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(Draw.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    
    public static void printSummary(int pairno, int start, int end){
        
        if(start > end){
            int temp = start;
            start = end;
            end = temp;
        }
        String f1,f2;
        f1 = (pairno==0)? "Summ_All" : ("Summ_Pair" + pairno);
        f2 = (start==end)? ("_R" + start + ".htm") : ("_R" + start + "_R" + end + ".htm");
        String filename = f1 + f2; 
        File outputfile;
        outputfile = new File(SwissPairs.homepath + filename );
        
       // String td = "</td><td>";
       // String row = "<tr><td>";
       // String rowend = "</td></tr>";
        String str;
        
         str = "<html><style>"
                 + "* {box-sizing: border-box;}"
                 + "table {font-family: arial, sans-serif;border-collapse: collapse;width: 75%;}"
                 + "td, th {border: 1px solid #dddddd;text-align: center; width: auto; padding: 8px;}"
                 + "div { margin: 20px;}"
                 + ".column {float: left; width: 40%; padding: 30px;}"
                 + ".row:after {content: '';display: table;  clear: both;  padding: 30px;}"
                 + "</style>"
                 + "<body><div>\n\n";
         
         if(pairno == 0){
             for(int i = 1; i <= SwissPairs.pairs; i++){
                 if(SwissPairs.withdrawn[i] == false){
                    for(int j = start; j <= end; j++){
                        str += indsumm(i,j);
                        str += "\n<br/><br/>\n";
                    }
                 }
             }             
         }
         
         else {
             if(SwissPairs.withdrawn[pairno] == false){
                    for(int j = start; j <= end; j++){
                        str += indsumm(pairno,j);
                        str += "\n<br/><br/>\n";
                    }
                 }
         }
         
         
         str += "</p><br/><br/></div></body></html>";
         try {
            FileWriter fr = new FileWriter(outputfile);
            fr.write(str);
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(Draw.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }    
    
    public static String indsumm(int pairno, int rno){
        
        if(SwissPairs.drawhistory[pairno][rno] == 0) return "";
        Boolean ns = false;
        String str = "";
        //String opps;
        int oppno;
        int mul = 1;
        str += "<h4>" + pairno + ": " + SwissPairs.pairnames[pairno][0] + " - " + SwissPairs.pairnames[pairno][1] + "</h4>\n";
        str += "<p>Round <strong>" + rno + "</strong>&nbsp; &nbsp; &nbsp; &nbsp; Played As: ";
        
        if(SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].nsno == pairno){
            ns = true;
            str += "<strong>NS</strong>&nbsp; &nbsp; &nbsp; &nbsp;";
            oppno = SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].ewno;
        }
        else {
            ns = false;
            str += "<strong>EW</strong>&nbsp; &nbsp; &nbsp; &nbsp;";
            oppno = SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].nsno;
            mul = -1;
        }
        double vp =  SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].vp;
        vp = (ns) ? vp : (20 - vp);    
        
        String td = "</td><td>";
        String row = "<tr><td>";
        String rowend = "</td></tr>";
        
        str += "vs. " + oppno + ": " + SwissPairs.pairnames[oppno][0] + " - " + SwissPairs.pairnames[oppno][1] + "<br/><br/>\n";
        
          
        if(!SwissPairs.usingBM){
            str+= "<table style=\"width:500px\"><th>Bd</th><th>Score</th><th>Datum</th><th>IMPs</th>\n";
            for(int i = 0; i < SwissPairs.bdsperrnd; i++){
            str += row + SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][0] +
                   td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][1] * mul) +
                   td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][3] * mul) +
                   td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][2] * mul) +
                    rowend + "\n";           
            }
            str += row + "</td><td colspan='2'> VPs:  " + String.format("%.2f", vp) + 
                 td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].imps * mul) +
                 rowend + "</table><br/>\n";
        }
        else {
            str += "<table style=\"width:500px\"><th>Bd</th><th>Contr</th><th>By</th><th>Res</th><th>Score</th><th>Datum</th><th>IMPs</th>\n";
            for(int i = 0; i < SwissPairs.bdsperrnd; i++){
                str += row + SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][0] +
                       td + SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].contract[i][0] +
                       td + SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].contract[i][1] +
                       td + SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].contract[i][2] +
                       td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][1] * mul) +
                       td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][3] * mul) +
                       td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].table[i][2] * mul) +
                        rowend + "\n";           
            }
            str += row + td + td + "</td><td colspan='2'> VPs:  " + String.format("%.2f", vp) + 
                   td + td + (SwissPairs.mss[rno].rss[SwissPairs.drawhistory[pairno][rno]].imps * mul) +
                   rowend + "</table><br/>\n";
        }
        
        
       
        str += "Total Score: <strong>" + String.format("%.2f", SwissPairs.crosstable[pairno][0]) + "<strong> (Penalty: " + SwissPairs.penalties[pairno] + " VPs)<br/>";
        
        return str;
    }
    
    public static void printRoundResult(int rno){
        String td = "</td><td>";
        String row = "<tr><td>";
        String rowend = "</td></tr>";
        String filename = "Results_for_R" + rno + ".htm"; 
        File outputfile;
        outputfile = new File(SwissPairs.homepath + filename );
        String str;
        double vp;
        String p1, p2;
        Boolean ns;
        
         str = "<html><style>"
                 + "* {box-sizing: border-box;}"
                 + "table {font-family: arial, sans-serif;border-collapse: collapse;width: 75%;}"
                 + "td, th {border: 1px solid #dddddd;text-align: center; width: auto; padding: 8px;}"
                 + "div { margin: 20px;}"
                 + ".column {float: left; width: 40%; padding: 30px;}"
                 + ".row:after {content: '';display: table;  clear: both;  padding: 30px;}"
                 + "</style>"
                 + "<body><div>\n\n";

        //str += "<h3>" + SwissPairs.nameOfEvent + "</h3> \n\n";
        str += "<h3>Results for Round " + rno + "</h3></div>\n\n";
        
        str += "<p><table style=\"width:500px\"><th>Pair</th><th>Names</th><th>VPs</th>";
        
        for(int i = 1; i <= SwissPairs.pairs; i++){
            if(SwissPairs.drawhistory[i][rno] == 0) continue;
            p1 = SwissPairs.pairnames[i][0];
            p2 = SwissPairs.pairnames[i][1];
            ns = false;
            if(SwissPairs.mss[rno].rss[SwissPairs.drawhistory[i][rno]].nsno == i) ns = true;
            vp = SwissPairs.mss[rno].rss[SwissPairs.drawhistory[i][rno]].vp;
            vp = (ns) ? vp : (20 - vp);
            str += row + i + td + p1 + " - " + p2 + td + String.format("%.2f", vp) + rowend;
        }
        str += "</table></p><br/><br/></div></body></html>";
        try {
            FileWriter fr = new FileWriter(outputfile);
            fr.write(str);
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(Draw.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void errorDialog(String str){
        final Stage dialogStage = new Stage();
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initModality(Modality.APPLICATION_MODAL);      
        

        Label errorLabel = new Label("Could not make a valid Draw!");
        
        Text tx = new Text(str);

        Button okBtn = new Button("OK");
        okBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {  
                dialogStage.close();
            }
        });

        VBox vBox = new VBox();
        vBox.setSpacing(10.0);
        vBox.setPadding(new Insets(25, 25, 25, 25));
        vBox.getChildren().addAll(errorLabel, tx, okBtn);

        dialogStage.setScene(new Scene(vBox));
        dialogStage.showAndWait();
    //Thanks to: AJJ on StackOverflow https://stackoverflow.com/questions/22716249/javafx-dialog-box
    }
    
    public static void fullRecap(){
        String td = "</td><td>";
        
        String row = "<tr><td>";
        String rowend = "</td></tr>";
        
        
        
        String filename = "Recap.htm"; 
        File outputfile;
        outputfile = new File(SwissPairs.homepath + filename );
        String str;
        
        int[] vultable = {1,0,2,1,3,2,1,3,0,1,3,0,2,3,0,2,1};
        String[] vulstrings = {"None", "EW", "NS", "Both"}; 
        String[] dealertable = {"W", "N", "E", "S"};
        
         str = "<html><style>"
                 + "* {box-sizing: border-box;}"
                 + "table {font-family: arial, sans-serif;border-collapse: collapse;width: 75%;}"
                 + "td, th {border: 1px solid #dddddd;text-align: center; width: auto; padding: 8px;}"
                 + "div { margin: 20px;}"
                 + ".column {float: left; width: 40%; padding: 30px;}"
                 + ".row:after {content: '';display: table;  clear: both;  padding: 30px;}"
                 + "</style>"
                 + "<body><div>\n\n";

        //Final Ranking
        str += "<h3>Leaders after Round " + SwissPairs.rndsScored + ":</h3></div>\n\n";
        
        str += "<p><table style=\"width:500px\"><th>Rank</th><th>No.</th><th>Name</th><th>VP</th>"; 
        int pairno;
        String vpstr;
        for(int i = 1; i<= (SwissPairs.pairs - SwissPairs.withdrawnpairs); i++){
            pairno = SwissPairs.leaders[i];
            //vpstr = String.valueOf(SwissPairs.crosstable[pairno][0]);
            vpstr = String.format("%.2f", SwissPairs.crosstable[pairno][0]);
            str += row + i + td + pairno + td + SwissPairs.pairnames[pairno][0] + 
                    " - " + SwissPairs.pairnames[pairno][1] + td + vpstr + rowend;
        }
        
        str += "</table></p><br/><hr/></div>";
        
        // Board by board
        int bds = SwissPairs.bdsperrnd*SwissPairs.rndsScored;
        String[][] deals = new String[bds + 1][16];
        inputDeals(deals);
        if("false".equals(deals[0][0])) return;
        
        String tdleft = "</td><td style='text-align:left; background-color:e9ebb2; letter-spacing: 2px;font-weight: bold;'>";
        String br = "<br />";       
        String s = "&spades; ";
        String h = "<span style='color:red'>&hearts; </span>";
        String d = "<span style='color:red'>&diams; </span>";
        String c = "&clubs; ";
        String temp[];
        String contract = "";
        
        for(int i = 1, rno, rbdno, tbls; i <= bds; i++){
            
            rno = (i-1)/SwissPairs.bdsperrnd + 1;
            rbdno = (i-1)%SwissPairs.bdsperrnd;
           // System.out.println(i +  " " + rno + " " + rbdno);
            tbls = SwissPairs.mss[rno].tables;
            
            str+= "<div class='row'><h3>Board " + i + "</h3>";
            str+= "<div class='column'><table>" + 
                    "<th>Table</th><th>NS</th><th>EW</th><th>Contract</th><th>By</th><th>Res</th><th>NS</th><th>EW</th>";
            
            String nsscore, ewscore;
            int datum = 0;
            for(int tbl = 1 ; tbl <= tbls; tbl++){
                if(SwissPairs.mss[rno].rss[tbl].table[rbdno][1] > 0){
                    nsscore = Integer.toString(SwissPairs.mss[rno].rss[tbl].table[rbdno][1]);
                    ewscore = "";
                }
                else {
                    ewscore = Integer.toString(SwissPairs.mss[rno].rss[tbl].table[rbdno][1] * -1);
                    nsscore = "";
                }
                temp = SwissPairs.mss[rno].rss[tbl].contract[rbdno][0].split(" ");
                if(temp.length > 1){
                    if("S".equals(temp[1])) contract = temp[0] + s;
                    else if("H".equals(temp[1])) contract = temp[0] + h; 
                    else if("D".equals(temp[1])) contract = temp[0] + d; 
                    else if("C".equals(temp[1])) contract = temp[0] + c; 
                    else contract = temp[0] + "NT"; 
                }
                else contract = temp[0];
                if(temp.length == 3) contract = contract + temp[2];
                str+= row + tbl + td +  SwissPairs.mss[rno].rss[tbl].nsno + td + 
                       SwissPairs.mss[rno].rss[tbl].ewno + td + contract + td + 
                        SwissPairs.mss[rno].rss[tbl].contract[rbdno][1] + td + 
                        SwissPairs.mss[rno].rss[tbl].contract[rbdno][2] + td + 
                        nsscore + td + ewscore + rowend;
                datum = SwissPairs.mss[rno].rss[tbl].table[rbdno][3];
            }
            str += "</table></div><div class='column'>";
            
            
            str += "<table style='border: 2px solid #91ff47;'><td style='text-align: left;'>";  
            str += "Dealer: " + dealertable[i%4] + "<br/><br/> Vul: " + vulstrings[vultable[i%16]] + "</td>";        
            
            str +=  tdleft + s + deals[i][0] + br + h + deals[i][1] + br + d + deals[i][2] + br + c + deals[i][3] + br + td + rowend +
                    tdleft + s + deals[i][12] + br + h + deals[i][13] + br + d + deals[i][14] + br + c + deals[i][15] + br + td + 
                    tdleft + s + deals[i][4] + br + h + deals[i][5] + br + d + deals[i][6] + br + c + deals[i][7] + br + rowend + td +                    
                    tdleft + s + deals[i][8] + br + h + deals[i][9] + br + d + deals[i][10] + br + c + deals[i][11] + br + td + rowend;
            
            str += "</table>";
            str += "<br/><p style=' font-size:30px;'>Datum: " + datum + "</p></div></div>\n\n";
            if(i%SwissPairs.bdsperrnd == 0) str += "<hr/>";
            
        }
        str += "<br/><br/></body></html>";
        
        try {
            FileWriter fr = new FileWriter(outputfile);
            fr.write(str);
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(Draw.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void inputDeals(String[][] dealrecords){
        /*returns an array of deal records:
        Each deal record is arranged in the following way:
        0 -> North Spades
        1 -> North Hearts
        2, 3, 
        4 -> East Spades
        5,6,7
        8 to 11 -> South
        12 to 15 -> West
        
        eg dealrecords[15][10] stores South's diamond holding from deal no. 15
        */
        int noofdeals = SwissPairs.bdsperrnd*SwissPairs.rndsScored;
        //String[][] dealrecords = new String[noofdeals + 1][16];
        
        try{
            File inputfile = new File(SwissPairs.homepath + SwissPairs.dealfilename);
            FileReader fr = new FileReader(inputfile);
            BufferedReader br = new BufferedReader(fr);
            String ist;
            String deals[] = new String[4];
            String temp[];
            int i = 1;
            String[] north;
            String[] south;
            String[] east;
            String[] west;
            
            while ((ist = br.readLine()) != null) {
                if(!("".equals(ist))){                    
                    if("[Deal ".equals(ist.substring(0,6))){	
                        System.out.println(i + ist); 
                        temp = ist.split("\"");
                        temp = temp[1].split(":");
                        if("N".equals(temp[0])){
                            temp = temp[1].split(" ");
                            deals[0] = temp[0];
                            deals[1] = temp[1];
                            deals[2] = temp[2];
                            deals[3] = temp[3];
                        }	
                        else if("E".equals(temp[0])){
                            temp = temp[1].split(" ");
                            deals[0] = temp[3];
                            deals[1] = temp[0];
                            deals[2] = temp[1];
                            deals[3] = temp[2];
                        }
                        else if("S".equals(temp[0])){
                            temp = temp[1].split(" ");
                            deals[0] = temp[2];
                            deals[1] = temp[3];
                            deals[2] = temp[0];
                            deals[3] = temp[1];
                        }
                        else {
                            temp = temp[1].split(" ");
                            deals[0] = temp[1];
                            deals[1] = temp[2];
                            deals[2] = temp[3];
                            deals[3] = temp[0];
                        }						
                        north = deals[0].split("\\.");                        
                        east = deals[1].split("\\.");
                        south = deals[2].split("\\.");
                        west = deals[3].split("\\.");

                        System.arraycopy(north, 0, dealrecords[i], 0, north.length);
                        System.arraycopy(east, 0, dealrecords[i], 4, east.length);
                        System.arraycopy(south, 0, dealrecords[i], 8, south.length);
                        System.arraycopy(west, 0, dealrecords[i], 12, west.length);

                        i = i+1;
                        if(i > noofdeals) return;
                    }



                }				


            }
        }
	catch (Exception e){
            dealrecords[0][0] = "false";
            return;
        }
        
        return;
        
    }
}
