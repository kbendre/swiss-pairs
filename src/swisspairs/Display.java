/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Kaustubh Bendre
 */
public class Display {
    public static ScrollPane Display(int rno) {
        //int pairs = SwissPairs.pairs;
       // int tables = SwissPairs.tables;
        int tables = SwissPairs.mss[rno].rss.length - 1;
        ///debug
        //int rno = 1;
        ///debug
        
        ScrollPane scrollPane = new ScrollPane();
        GridPane maingrid = new GridPane();      
        maingrid.setAlignment(Pos.TOP_LEFT);
        maingrid.setHgap(10);
        maingrid.setVgap(10);
        maingrid.setPadding(new Insets(25, 25, 25, 25));
        
        GridPane[] grid = new GridPane[tables];
        for(int i = 0; i < tables; i++){
            grid[i] = new GridPane();
            grid[i].setAlignment(Pos.TOP_LEFT);
            grid[i].setHgap(10);
            grid[i].setVgap(10);
            grid[i].setPadding(new Insets(25, 25, 25, 25));
        }
        Text scenetitle = new Text("Round " + rno + ":");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        maingrid.add(scenetitle, 0, 0, 2, 1);    
        
        Label walabel = new Label("Wrong Axis on Table:");
        walabel.setFont(Font.font("Tahoma", FontWeight.NORMAL, 15));
        
       // maingrid.add(walabel, 0, 1);
        
        Spinner<Integer> waSpinner = new Spinner(0, SwissPairs.tables, 0);
        waSpinner.setPrefWidth(80);
      //  maingrid.add(waSpinner, 1, 1);
        
        Button wabtn = new Button();
        wabtn.setText("Change Axis");              
      //  maingrid.add(wabtn, 2, 1);
      //  GridPane.setHalignment(wabtn, HPos.RIGHT);
        wabtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int tno = waSpinner.getValue();
                int ns,ew,temp;
                double oldnsscore,oldewscore;
                if(tno != 0){
                    ns = SwissPairs.mss[rno].rss[tno].nsno;
                    ew = SwissPairs.mss[rno].rss[tno].ewno;
                    
                    oldnsscore = SwissPairs.crosstable[ns][ew];
                    oldewscore = SwissPairs.crosstable[ew][ns];
           
                    SwissPairs.crosstable[ns][ew] = oldewscore;
                    SwissPairs.crosstable[ew][ns] = oldnsscore;
            
                    SwissPairs.crosstable[ns][0] -= oldnsscore;
                    SwissPairs.crosstable[ew][0] -= oldewscore;
            
                    SwissPairs.crosstable[ns][0] += oldewscore;       
                    SwissPairs.crosstable[ew][0] += oldnsscore;  
                    
                    temp = ns;
                    ns = ew;
                    ew = temp;
                    
                    SwissPairs.mss[rno].rss[tno].nsno = ns;
                    SwissPairs.mss[rno].rss[tno].ewno = ew;
                    
                    SwissPairs.displaytab.setContent(Display.Display(rno));
                    SwissPairs.tabPane.getSelectionModel().select(2);
                }
            }
            });
        
        HBox hb = new HBox();
        hb.setSpacing(10.0);
        hb.setPadding(new Insets(25, 25, 25, 25));
        hb.getChildren().addAll(walabel,waSpinner,wabtn);
        hb.setAlignment(Pos.CENTER_LEFT);
        hb.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid outside;" + 
                      "-fx-border-width: 1;" +
                      "-fx-border-insets: 15;" +                       
                      "-fx-border-color: darkgray;");
        
        maingrid.add(hb, 0, 1, 2, 1);
        
        int i, j, k;
        Text tx;
        
        for (i=0; i < tables; i++){
            grid[i].add(new Text("Table " + (i+1)), 0, 0);
            grid[i].add(new Text("NS: " + SwissPairs.mss[rno].rss[i+1].nsno), 0, 1);
            grid[i].add(new Text("EW: " + SwissPairs.mss[rno].rss[i+1].ewno), 1, 1);
            grid[i].add(new Text("Bd No."), 0, 2);
            grid[i].add(new Text("Score"), 1, 2);
            grid[i].add(new Text("IMPs"), 2, 2);
            grid[i].add(new Text("Datum"), 3, 2);
            for(j=0;j<SwissPairs.bdsperrnd;j++){
                for(k=0; k<3; k++){
                    tx = new Text("" + SwissPairs.mss[rno].rss[i+1].table[j][k]);
                    tx.setTextAlignment(TextAlignment.JUSTIFY);
                    grid[i].add(tx, k, j + 3);
                    GridPane.setHalignment(tx, HPos.RIGHT);
                }
                tx = new Text("" + Score.datums[j]);
                tx.setTextAlignment(TextAlignment.RIGHT);
                
                //tx.setPrefColumnCount(14);
                grid[i].add(tx, 3, j + 3);
                GridPane.setHalignment(tx, HPos.RIGHT);
            }            
            Button btn = new Button();
            btn.setText("Edit");    
            btn.setUserData(i);
            grid[i].add(btn, 3, 1);
            GridPane.setHalignment(btn, HPos.RIGHT);
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {     
                    SwissPairs.editscore_score = 10000;
                    dialog(rno);
                    if(SwissPairs.editscore_score != 10000){
                        int tno = (int) btn.getUserData();
                        int bdno;
                        bdno = SwissPairs.editscore_bdno - SwissPairs.mss[rno].rss[1].table[0][0];
                        SwissPairs.mss[rno].rss[tno + 1].table[bdno][1] = SwissPairs.editscore_score;
                        SwissPairs.mss[rno].rss[tno + 1].contract[bdno][0] = "-";
                        SwissPairs.mss[rno].rss[tno + 1].contract[bdno][1] = "-";       
                        SwissPairs.mss[rno].rss[tno + 1].contract[bdno][2] = "-";
                        Score.Score(rno, true);
                        Draw.printSummary(0, rno, rno);
                        Draw.printRoundResult(rno);
                    }
                    SwissPairs.displaytab.setContent(Display.Display(rno));
                    SwissPairs.tabPane.getSelectionModel().select(2);
                }
            });
            grid[i].setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid outside;" + 
                      "-fx-border-width: 1;" +
                      "-fx-border-insets: 15;" +                       
                      "-fx-border-color: darkgray;");
        }
        
        for(i=0, j=0; i< tables; i++){
            if (j == 2) j = 0;
            maingrid.add(grid[i], j, i/2 + 2);
            j++;
        }
        
        
        scrollPane.setContent(maingrid);
        return scrollPane;
    

    }
    public static void dialog(int rno){
        final Stage dialogStage = new Stage();
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        
        int fboard = SwissPairs.mss[rno].rss[1].table[0][0];

        Label bdLabel = new Label("Board No:");  
        Spinner<Integer> bdnoSpinner = new Spinner(fboard, fboard + SwissPairs.bdsperrnd - 1, fboard);

        Label newscoreLabel = new Label("New NS Score:");
        TextField tf = new TextField();
        tf.setPrefWidth(5);
        
        Text tx = new Text("");

        Button okBtn = new Button("OK");
        okBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) { 
                String str = tf.getText();
                if(str.matches("[+-]?\\d+")){ 
//shuangwhywhy, Damiii, Shon Vella : https://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
                    SwissPairs.editscore_score = Integer.parseInt(str);
                    SwissPairs.editscore_bdno = bdnoSpinner.getValue();
                }
                else {
                    tx.setText("Invalid input");
                    SwissPairs.editscore_score = 10000;
                }
                
                dialogStage.close();
            }
        });

        VBox vBox1 = new VBox();
        vBox1.setSpacing(10.0);
        vBox1.setPadding(new Insets(25, 25, 25, 25));
        vBox1.getChildren().addAll(bdLabel, bdnoSpinner);

        VBox vBox2 = new VBox();
        vBox2.setSpacing(10.0);
        vBox2.setPadding(new Insets(25, 25, 25, 25));
        vBox2.getChildren().addAll(newscoreLabel, tf, tx);

        VBox vBox = new VBox();
        vBox.setSpacing(10.0);
        vBox.setPadding(new Insets(25, 25, 25, 25));
        vBox.getChildren().addAll(vBox1, vBox2, okBtn);

        dialogStage.setScene(new Scene(vBox));
        dialogStage.showAndWait();
    //Thanks to: AJJ on StackOverflow https://stackoverflow.com/questions/22716249/javafx-dialog-box
    }
}
