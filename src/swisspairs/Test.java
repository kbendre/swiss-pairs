/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Kaustubh Bendre
 */
public class Test {
    public static void Test() throws IOException{
        testScore();
    }

    public static void testMakeDraw() {
            System.out.println("makeDraw");
            int[] inputarray = {0,1,2,3,4,5,6,7,8,9};
            SwissPairs.crosstable[1][2] = 1;
            SwissPairs.crosstable[1][4]=1;
            SwissPairs.crosstable[1][3] = 1;
            SwissPairs.crosstable[9][SwissPairs.MAX_PAIRS-1] = 1;
            SwissPairs.crosstable[8][SwissPairs.MAX_PAIRS-1] = 1;
            SwissPairs.crosstable[7][SwissPairs.MAX_PAIRS-1] = 1;
            SwissPairs.crosstable[6][SwissPairs.MAX_PAIRS-1] = 1;
            //inputarray = 
            int pairs = 9;
           // int[] expResult = {1,1,2,3,4,5,6,7,8,9,10};
            int[] result = Draw.makeDraw(inputarray, pairs);
            int[] expResult = {1,1,2,3,4,5,6,7,8,9,10};
            for(int i=0;i<=pairs;i++) System.out.print(result[i] + " ");
        }
    
    public static void inputScore(int rno) throws IOException{
        File inputfile = new File("C:\\BAM\\sp.csv");
        BufferedReader br;
        br = new BufferedReader(new FileReader(inputfile));
        
        String ist;
        String[] temp;
        int t = 1;
        
        while ((ist = br.readLine()) != null) {
            temp = ist.split(",");
            for(int j = 0; j < SwissPairs.bdsperrnd; j++){
                SwissPairs.mss[rno].rss[t].table[j][1] = Integer.parseInt(temp[j]);
            }
            t++;
        }  
        br.close();
        
    }
    
    public static void testScore() throws IOException{
     //   System.out.println("Score");
     //   for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(SwissPairs.crosstable[i][0] + ", ");
     //   System.out.print("\n");        
    //    for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(SwissPairs.leaders[i] + ", ");
     //   System.out.print("\n");   
        Score.allocSS(1);
        inputScore(1);
        Score.Score(1, true);
     //   for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(String.format("%.2f", SwissPairs.crosstable[i][0]) + ", ");
     //   System.out.print("\n");         
     //   for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(SwissPairs.leaders[i] + ", ");
     //   System.out.print("\n");  
    //    for(int i=0;i<SwissPairs.bdsperrnd;i++) System.out.print(Score.datums[i] + ", ");
    //    System.out.print("\n");  
        SwissPairs.displaytab.setContent(Display.Display(1));
        SwissPairs.displaytab.setDisable(false);
        
    }
    public static void testDB() throws SQLException{
        PreparedStatement ps = null;
        String query;
        ResultSet rs = null;
        Connection conn  =DriverManager.getConnection("jdbc:ucanaccess://" + SwissPairs.homepath + SwissPairs.dbfile + ";newdatabaseversion=V2000");
        
        try{
            BM.fillDB(conn);
        }
        
        catch (Exception e) {
			e.printStackTrace();
	}
        
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /*ignored*/ }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /*ignored*/ }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /*ignored*/ }
            }
        }
    }
}


/*Draw.Draw();
        System.out.println("Draw");
        Score.Score(2);
        for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(String.format("%.2f", SwissPairs.crosstable[i][0]) + ", ");
        System.out.print("\n");         
        for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(SwissPairs.leaders[i] + ", ");
        System.out.print("\n");  
        for(int i=0;i<SwissPairs.bdsperrnd;i++) System.out.print(Score.datums[i] + ", ");
        System.out.print("\n");  
        Draw.Draw();
        System.out.println("Draw");
        Score.Score(3);
        for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(String.format("%.2f", SwissPairs.crosstable[i][0]) + ", ");
        System.out.print("\n");         
        for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(SwissPairs.leaders[i] + ", ");
        System.out.print("\n");  
        for(int i=0;i<SwissPairs.bdsperrnd;i++) System.out.print(Score.datums[i] + ", ");
        System.out.print("\n");  
        Draw.Draw();
        System.out.println("Draw");
        Score.Score(4);
        for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(String.format("%.2f", SwissPairs.crosstable[i][0]) + ", ");
        System.out.print("\n");         
        for(int i=0;i<=SwissPairs.pairs;i++) System.out.print(SwissPairs.leaders[i] + ", ");
        System.out.print("\n");  
        for(int i=0;i<SwissPairs.bdsperrnd;i++) System.out.print(Score.datums[i] + ", ");
        System.out.print("\n");  */