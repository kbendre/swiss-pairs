/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Kaustubh Bendre
 */
public class Adjustment {
    public static void dialog (){
    final Stage dialogStage = new Stage();
                //dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.initModality(Modality.APPLICATION_MODAL);
                
                Label penLabel = new Label("Apply Adjustment:");
                penLabel.setAlignment(Pos.BASELINE_CENTER);

                Label pairnoLabel = new Label("Pair No.:");
                Spinner<Integer> pairnoSpinner = new Spinner(1, SwissPairs.pairs, 1);
                
                Label penamountLabel = new Label("Adjustment to be applied:");
                Spinner<Double> adjSpinner = new Spinner(-10, 10, 0, 0.5);
                
                Button okBtn = new Button("OK");
                okBtn.setOnAction(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent arg0) {
                        SwissPairs.penalties[pairnoSpinner.getValue()] += adjSpinner.getValue();
                        SwissPairs.crosstable[pairnoSpinner.getValue()][0] += adjSpinner.getValue();
                       // SwissPairs.namestab.setContent(Names.Names());
                        dialogStage.close();

                    }
                });
                
                VBox vBox1 = new VBox();
                vBox1.setSpacing(10.0);
                vBox1.setPadding(new Insets(25, 25, 25, 25));
                vBox1.getChildren().addAll(pairnoLabel, pairnoSpinner);

                VBox vBox2 = new VBox();
                vBox2.setSpacing(10.0);
                vBox2.setPadding(new Insets(25, 25, 25, 25));
                vBox2.getChildren().addAll(penamountLabel, adjSpinner);

                VBox vBox = new VBox();
                vBox.setSpacing(20.0);
                vBox.setPadding(new Insets(25, 25, 25, 25));
                vBox.getChildren().addAll(penLabel, vBox1, vBox2, okBtn);

                dialogStage.setScene(new Scene(vBox));
                dialogStage.show();
    //Thanks to: AJJ on StackOverflow https://stackoverflow.com/questions/22716249/javafx-dialog-box
    //Also APPLICATION_MODAL is probably what you want: dmolony
    
}
}