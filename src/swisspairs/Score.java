/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Kaustubh Bendre
 */
public class Score {
    public static int rndsScored = SwissPairs.rndsScored;
   // public static int size = SwissPairs.tables * SwissPairs.bdsperrnd; 
    public static int[] datums; 
    
    public static int id[]; 
    public static int table[];
    public static int level[];    
  //  public static int pairns[];    
  //  public static int pairew[];    
    public static String contract[];
    public static String strain[];
    public static String made[];
    public static String decl[];
    public static String dblstr[];
    public static int board[];
    public static int round[];
    public static int nsscore[];
    public static int nsmp[];
    
    
    public static Boolean Score(int rndno, Boolean rescore) {
        int pairs = SwissPairs.pairs - SwissPairs.withdrawnpairs;
        int tables = (pairs % 2 == 0)? pairs/2 : (pairs-1)/2;
        int bds = SwissPairs.bdsperrnd;
        int expectedsize = tables * bds;
        int size = 0; 
        int bdno;
        //int rno = SwissPairs.rndsScored + 1;
       //Fill in the scoresheets
        if(!rescore){
            try {            
                if(SwissPairs.usingBM)    size = retrieveScores(rndno);
                else {
                    size = retrieveScoresfromFile(rndno);

                    if (size < expectedsize) {
                        errorDialog("All scores not entered (" + size + " of " + expectedsize +" retrieved).");
                        return false;   
                    }
                }
            } catch (ClassNotFoundException | SQLException | IOException ex) {
                Logger.getLogger(Score.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            for(int i = 0; i < size; i++){
               // bdno = board[i] - SwissPairs.firstbd;//wrong, in case we are rescoring
                bdno = board[i] - SwissPairs.firstbd[rndno];
                SwissPairs.mss[rndno].rss[table[i]].table[bdno][1] = nsscore[i];
                SwissPairs.mss[rndno].rss[table[i]].contract[bdno][0] = contract[i];
                SwissPairs.mss[rndno].rss[table[i]].contract[bdno][1] = decl[i];
                SwissPairs.mss[rndno].rss[table[i]].contract[bdno][2] = made[i];            
            }
        }
        
        double c1 = tables/5.0;
        int cutoff = (int) Math.floor(c1 + 0.5);
       // System.out.println(c1 + " " + cutoff);
        datums = new int[bds];
        int[] x;        
        int sum, ns, ew;
        double vp, oldnsscore, oldewscore;
        
        for(int i=0; i<bds; i++){//calculate datums and fill column (2) in scoresheets
            x = new int[tables];
            
            for(int j=0;j<tables; j++){
                x[j] = SwissPairs.mss[rndno].rss[j+1].table[i][1];
            }
            Arrays.sort(x);
            
            sum = 0;
            for(int j=cutoff; j<tables-cutoff;j++){
                sum += x[j];
            }
            
            //find datum, rounded to nearest 10
            double temp = (sum < 0) ? sum * -1 : sum;
            temp = Math.floor((temp/(tables - 2*cutoff)) + 0.5);            
            temp = Math.floor((temp + 5)/10) * 10;
            temp = (sum < 0) ? temp * -1 : temp;
           
            datums[i] = (int) temp;        
            
            for(int j=1;j<=tables; j++){
                SwissPairs.mss[rndno].rss[j].table[i][2] = datumtoIMP(datums[i], SwissPairs.mss[rndno].rss[j].table[i][1]);
                SwissPairs.mss[rndno].rss[j].table[i][3] = datums[i];
            }            
        }
        
        for(int i = 1; i <= tables; i++){
            sum = 0;
            for(int j = 0; j < bds; j++){
                sum += SwissPairs.mss[rndno].rss[i].table[j][2];
            }
            SwissPairs.mss[rndno].rss[i].imps = sum;
            vp = imptoVP(sum);
            SwissPairs.mss[rndno].rss[i].vp = vp;
          //  vp = Math.round(vp,2);
            ns = SwissPairs.mss[rndno].rss[i].nsno;
            ew = SwissPairs.mss[rndno].rss[i].ewno;
            
            if(rescore){
                oldnsscore = SwissPairs.crosstable[ns][ew];
                oldewscore = SwissPairs.crosstable[ew][ns];
                SwissPairs.crosstable[ns][0] -= oldnsscore;//in case we are rescoring the round
                SwissPairs.crosstable[ew][0] -= oldewscore;//
            }
            
            SwissPairs.crosstable[ns][ew] = vp;
            SwissPairs.crosstable[ew][ns] = 20 - vp;
            
            
            
            SwissPairs.crosstable[ns][0] += vp;       //totals are stored in the 0th column
            SwissPairs.crosstable[ew][0] += 20 - vp;  //
        }
        
        //if there is a bye
        if(pairs%2 == 1){
            ns = SwissPairs.newdraw[pairs];
           
            if(rescore) {
                oldnsscore = SwissPairs.crosstable[ns][SwissPairs.MAX_PAIRS - 1];
                SwissPairs.crosstable[ns][0] -= oldnsscore;
            }//in case we are rescoring the round
            
            SwissPairs.crosstable[ns][SwissPairs.MAX_PAIRS-1] = 12;
            SwissPairs.crosstable[ns][0] += 12;
        }
        
        if (SwissPairs.rndsScored < rndno) SwissPairs.rndsScored = rndno;
        return true;
    }    
    
    public static void allocSS(int rndno){
        SwissPairs.mss[rndno] = new RoundScoreSheet();       
        SwissPairs.mss[rndno].fill(rndno);
        int pairs = SwissPairs.pairs - SwissPairs.withdrawnpairs;
        int tables = (pairs % 2 == 0)? pairs/2 : (pairs-1)/2;
                
        for(int i = 1; i <= tables; i++){
           SwissPairs.mss[rndno].rss[i] = new ScoreSheet();           
           SwissPairs.mss[rndno].rss[i].fill(i,SwissPairs.newdraw[i*2-1],SwissPairs.newdraw[i*2],SwissPairs.firstbd[rndno]);
        }        
    }
    
    public static void ScoreAll(){
        for(int i = 1; i <= SwissPairs.rndsScored; i++) Score(i, true);
    }
    
    public static void editScoreFile(int rno) throws IOException{
        int fbd = SwissPairs.firstbd[rno];
        int tbls = SwissPairs.tables;
        int bds = SwissPairs.bdsperrnd;
        String str = "";
        File scoresfile = new File(SwissPairs.homepath + "scores.csv");
        System.out.println("editScoreFile: " + rno);
        str += "Round," + rno + "\n";

        str += "Table,";
        for(int i = 0; i < bds - 1; i++) str+= "Board " + (i+fbd) +",";
        str += "Board " + (fbd + bds -1) + "\n";

        for(int i = 1; i <= tbls; i++) str+= i + ",\n";
        str+= "\n";
        try{
            if(rno == 1){
                if(scoresfile.exists()){
                    int i = 1;                
                    File bufile = new File(SwissPairs.homepath + "backup_" + i + "_scores.csv");
                    while(bufile.exists()){
                        i++;
                        //bufile = new File(SwissPairs.homepath + "backup_" + i + "_" + SwissPairs.dbfile);
                        bufile = new File(SwissPairs.homepath + "backup_" + i + "_scores.csv");
                        scoresfile.renameTo(bufile);
                        scoresfile.delete();
                    }
                }
                FileWriter fr = new FileWriter(scoresfile);
                fr.write(str);
                fr.close();
                
            }
            else{
                FileWriter fr = new FileWriter(scoresfile, true);
                fr.write(str);
                fr.close();
            }
        }
        catch (IOException ex) {
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static int retrieveScoresfromFile(int rno) throws ClassNotFoundException, IOException{
        int size = SwissPairs.tables * SwissPairs.bdsperrnd;
        int lineno = 0; //line number
        int i = 0; //array index
        id = new int[size];
        table = new int[size];         
        board = new int[size];
        level = new int[size];
        nsscore = new int[size];
        nsmp = new int[size];
        round = new int[size];
        contract = new String[size];
        decl = new String[size];
        dblstr = new String[size];
        made = new String[size];
        strain = new String[size];
        try {
            
            File scoresfile = new File(SwissPairs.homepath + "scores.csv");
            if(scoresfile.exists()){
                FileReader frd = new FileReader(scoresfile);
                BufferedReader brd = new BufferedReader(frd);
             //   System.out.println("ccc");
                String str,errstr;    
                String[] sa;
                int len = 1 + SwissPairs.bdsperrnd; //table no. followed by the scores
                
                while ((str=brd.readLine())!=null){
                    lineno++;
                    if(str.startsWith("Round")){
                        sa = str.split(",");
                        if(sa.length < 2){
                            errstr = "Round number not entered (line " + lineno + ")";
                            errorDialog(errstr);
                            return 0;
                        }
                        sa[1] = sa[1].trim();
                        if(!sa[1].matches("[+-]?\\d+")){
                        errstr = "Error on line " + lineno + ". Please check the round number entered.";//Error Message here
                        errorDialog(errstr);
                        return 0;
                        } 
                        if(Integer.parseInt(sa[1]) != rno) continue;
                        while ((str=brd.readLine())!=null){ //Read only the scores for relevant round
                            lineno++;
                            //System.out.println(str);
                            if(str.startsWith("Round") || str.isEmpty()) return i;
                            if(str.startsWith("Table")) continue;
                            sa = str.split(",");
                           
                            if(sa.length < len ){
                                errstr = "Round " + rno + ": Only " + (sa.length - 1) + " scores on line " + lineno;
                                errorDialog(errstr);
                                return 0;
                            }

                            for(int ii = 0; ii < len; ii++){
                                sa[ii] = sa[ii].trim();
                                if(!sa[ii].matches("[+-]?\\d+")){
                                errstr = "Round " + rno + ": Error on line " + lineno + ". Please check the scores entered.";
                                errorDialog(errstr);
                                return 0;
                                } 
                            }
                            
                            for(int j=1, k = Integer.parseInt(sa[0]); j < len; j++){
                                id[i] = i;                        
                                table[i] = k;
                                board[i] = SwissPairs.firstbd[rno] + j - 1;                
                                round[i] = rno;
                                contract[i] = "-";
                                decl[i] = "-"; 
                                made[i] = "-";  
                                nsscore[i] = Integer.parseInt(sa[j]);
                                i++;
                            }
                        }
                    }                
                }
            }
        }
        catch (IOException ex) {
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }
        return size;
        
    }
    
    public static void errorDialog(String str){
        final Stage dialogStage = new Stage();
        //dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initModality(Modality.APPLICATION_MODAL);      
        

        Label errorLabel = new Label("Error reading scores.csv");
        
        Text tx = new Text(str);

        Button okBtn = new Button("OK");
        okBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {  
                dialogStage.close();
            }
        });

        VBox vBox = new VBox();
        vBox.setSpacing(10.0);
        vBox.setPadding(new Insets(25, 25, 25, 25));
        vBox.getChildren().addAll(errorLabel, tx, okBtn);

        dialogStage.setScene(new Scene(vBox));
        dialogStage.showAndWait();
    //Thanks to: AJJ on StackOverflow https://stackoverflow.com/questions/22716249/javafx-dialog-box
    }
    
    public static int retrieveScores(int rno) throws ClassNotFoundException, SQLException{
        
      //  File f1;//debug
      //  f1=new File("c:\\BAM\\BAM_debug_" + BAM.date + ".csv");//debug  
      //  String str1 = "";//debug
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection conn = null;
        Connection conn1 = null;
        int size = 0;
        try{
            //int rno = SwissPairs.rndsScored + 1;
            //int size = 0;
            
            conn =DriverManager.getConnection("jdbc:ucanaccess://" + SwissPairs.homepath + SwissPairs.dbfile + ";immediatelyReleaseResources=true");           
            conn.createStatement();
            String query;
            String str[];                       
            
            query = "Select count(*) from ReceivedData where Round = " + rno;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()) {
                size = Integer.parseInt(rs.getString(1));
            }
            
            //if (size < SwissPairs.tables * SwissPairs.bdsperrnd) return 0;//commented out for debug
             
            query = "Select ID, [Table], Board, Contract, [NS/EW], Result from ReceivedData where Round = " + rno;            
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            id = new int[size];
            table = new int[size];
         //   pairns = new int[size];
         //   pairew = new int[size];
            board = new int[size];
            level = new int[size];
            nsscore = new int[size];
            nsmp = new int[size];
            round = new int[size];
            contract = new String[size];
            decl = new String[size];
            dblstr = new String[size];
            made = new String[size];
            strain = new String[size];
                           
            //to check if all scores have been retrieved
            Boolean[][] checkmatrix = new Boolean[SwissPairs.tables][SwissPairs.bdsperrnd]; 
                        
            int i = 0;
            int bdindex;
            while (rs.next()){
                id[i] = rs.getInt("ID");
                table[i] = rs.getInt("Table");
                board[i] = rs.getInt("Board");
            //    pairns[i] = rs.getInt("PairNS");
           //     pairew[i] = rs.getInt("PairEW");
                contract[i] = rs.getString("Contract");
                decl[i] = rs.getString("NS/EW"); 
                made[i] = rs.getString("Result");  
//                bdindex = board[i] - SwissPairs.mss[rno].rss[1].table[0][0];
           //     checkmatrix[table[i] - 1][bdindex] = true; //commented out for debug 
                i++;
            }
            int lastid = 0;
            if(id.length > 1) 
                lastid = id[i-1];           
            //System.out.println(lastid);
            conn1 =DriverManager.getConnection("jdbc:ucanaccess://" + SwissPairs.homepath + SwissPairs.dbfile + ";immediatelyReleaseResources=true");           
            
            Statement st = conn1.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            query = "Update ReceivedData Set Processed ='true' Where ID <=" + lastid;// '1'";
            st.executeUpdate(query);
           
        /*    
            //if all scores have not been retrieved, exit witout doing anything more
            for(i = 0; i < SwissPairs.tables; i++){
                for(int j = 0; j < SwissPairs.bdsperrnd; j++){
                    if (!checkmatrix[i][j]) return 0;
                }
            }
          */ //commented out for debug  
            for(i = 0; i < size; i++){
                str = contract[i].split(" ");
                switch (str.length) {
                    case 1:
                        level[i] = 0;
                        strain[i] = "";
                        dblstr[i] = "";
                        break;
                    case 2:
                        level[i] = Integer.parseInt(str[0]);
                        strain[i] = str[1];
                        dblstr[i] = "";
                        break;
                    default:
                        level[i] = Integer.parseInt(str[0]);
                        strain[i] = str[1];
                        dblstr[i] = str[2];
                        break;
                }
                
                if(level[i] == 0) nsscore[i] = 0; // all pass
                else nsscore[i] = calculateRawScore(level[i], strain[i], made[i], decl[i], dblstr[i], board[i]); 
            //    str1 += level[i] + " " + strain[i] + " " + made[i] + " " + nsscore[i];//debug
             //   str1 += "\n";//debug
             //TO DO: Put all the nsscores in scores.csv
            }           
            
          //  FileWriter fr1 = new FileWriter(f1);//debug
          //  fr1.write(str1);//debug
          //  fr1.close();//debug
        }
        catch (NumberFormatException | SQLException e) {             
            System.out.println("Update failed!");
	}
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /*ignored*/ }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /*ignored*/ }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /*ignored*/ }
            }
            if (conn1 != null) {
                try {
                    conn1.close();
                } catch (SQLException e) { /*ignored*/ }
            }
        }

        return size;
    }   
    
    public static double imptoVP(int imp){
        double[] bd4 = {10,10.61,11.2,11.76,12.29,12.8,13.28,13.74,14.18,14.6,15,15.38,15.74,16.09,16.42,16.73,17.03,17.31,17.59,17.84,18.09,18.33,18.55,18.76,18.97,19.16,19.34,19.52,19.69,19.85,20};
        double[] bd5 = {10,10.55,11.08,11.59,12.07,12.53,12.98,13.41,13.81,14.2,14.58,14.94,15.28,15.61,15.92,16.23,16.52,16.79,17.06,17.31,17.56,17.79,18.01,18.23,18.43,18.63,18.82,19,19.17,19.33,19.49,19.64,19.79,19.93,20};
        double[] bd6 = {10,10.5,10.99,11.46,11.9,12.33,12.75,13.15,13.53,13.9,14.25,14.59,14.92,15.24,15.54,15.83,16.11,16.38,16.64,16.89,17.12,17.35,17.58,17.79,17.99,18.19,18.38,18.56,18.73,18.9,19.06,19.22,19.37,19.51,19.65,19.78,19.91,20};
        double[] bd7 = {10,10.47,10.92,11.35,11.77,12.18,12.57,12.94,13.31,13.65,13.99,14.32,14.63,14.93,15.22,15.5,15.78,16.04,16.29,16.53,16.77,16.99,17.21,17.42,17.62,17.82,18.01,18.19,18.36,18.53,18.69,18.85,19,19.15,19.29,19.43,19.56,19.68,19.8,19.92,20};
        double[] bd8 = {10,10.44,10.86,11.27,11.67,12.05,12.42,12.77,13.12,13.45,13.78,14.09,14.39,14.68,14.96,15.23,15.5,15.75,16,16.23,16.46,16.68,16.9,17.11,17.31,17.5,17.69,17.87,18.04,18.21,18.37,18.53,18.68,18.83,18.97,19.11,19.24,19.37,19.5,19.62,19.74,19.85,19.95,20};
        double[] bd9 = {10,10.41,10.81,11.2,11.58,11.94,12.29,12.63,12.96,13.28,13.59,13.89,14.18,14.46,14.74,15,15.26,15.5,15.74,15.97,16.2,16.42,16.63,16.83,17.03,17.22,17.41,17.59,17.76,17.93,18.09,18.25,18.4,18.55,18.69,18.83,18.97,19.1,19.22,19.34,19.46,19.58,19.69,19.8,19.9,20};
        double[] bd10 = {10,10.39,10.77,11.14,11.5,11.85,12.18,12.51,12.83,13.14,13.43,13.72,14,14.28,14.54,14.8,15.05,15.29,15.52,15.75,15.97,16.18,16.39,16.59,16.78,16.97,17.16,17.34,17.51,17.68,17.84,18,18.15,18.3,18.44,18.58,18.71,18.84,18.97,19.1,19.22,19.33,19.44,19.55,19.66,19.76,19.86,19.96,20};
        
        int sc = imp > 0 ? imp : imp * -1;
        double vp;
        
        switch (SwissPairs.bdsperrnd){
            case 4:
                if (sc >= bd4.length) vp =  20;
                else vp =  bd4[sc];
                break;
            case 5:
                if (sc >= bd5.length) vp =  20;
                else vp =  bd5[sc];
                break;
            case 6:
                if (sc >= bd6.length) vp =  20;
                else vp =  bd6[sc];
                break;
            case 7:
                if (sc >= bd7.length) vp =  20;
                else vp =  bd7[sc];
                break;
            case 8:
                if (sc >= bd8.length) vp =  20;
                else vp =  bd8[sc];
                break;
            case 9:
                if (sc >= bd9.length) vp =  20;
                else vp =  bd9[sc];
                break;
            case 10:
                if (sc >= bd10.length) vp =  20;
                else vp =  bd10[sc];
                break;
            default:
                vp =  0;
        }
        return imp > 0 ? vp : 20 - vp;  
    }
    
    public static int datumtoIMP(int datum, int score){
        int diff = score - datum;
        int ad = Math.abs(diff);
        int retvalue;
        
        if(ad <= 10) retvalue =  0;
        else if(ad <= 40) retvalue =  1;
        else if(ad <= 80) retvalue =  2;
        else if(ad <= 120) retvalue =  3;
        else if(ad <= 160) retvalue =  4;
        else if(ad <= 210) retvalue =  5;
        else if(ad <= 260) retvalue =  6;
        else if(ad <= 310) retvalue =  7;
        else if(ad <= 360) retvalue =  8;
        else if(ad <= 420) retvalue =  9;
        else if(ad <= 490) retvalue =  10;
        else if(ad <= 590) retvalue =  11;
        else if(ad <= 740) retvalue =  12;
        else if(ad <= 890) retvalue =  13;
        else if(ad <= 1090) retvalue =  14;
        else if(ad <= 1290) retvalue =  15;
        else if(ad <= 1490) retvalue =  16;
        else if(ad <= 1740) retvalue =  17;
        else if(ad <= 1990) retvalue =  18;
        else if(ad <= 2240) retvalue =  19;
        else if(ad <= 2490) retvalue =  20;
        else if(ad <= 2990) retvalue =  21;
        else if(ad <= 3490) retvalue =  22;
        else if(ad <= 3990) retvalue =  23;
        else retvalue =  24;
        
        return diff > 0 ? retvalue : retvalue * -1;
    }
    
    public static int calculateRawScore(int level, String strain, String made, String decl, String dblstr, int board){
        //ALL PASS is not scored here
        int[] vultable = {1,0,2,1,3,2,1,3,0,1,3,0,2,3,0,2,1};
        int denom;
        int dbl = 0;
        int vul;
        int NSScoreconverter;
        int ot;
        int score = 0;
        board = board%16;
        
       //////// dbl        
        if ("x".equals(dblstr)) dbl = 1;
        else if ("xx".equals(dblstr)) dbl = 2; 
        
        
      ///////// declarer, vul  
        if("N".equals(decl) || "S".equals(decl)){
            if (vultable[board] == 0 || vultable[board] == 1) vul = 0;
            else vul = 1;            
            NSScoreconverter = 1;
        }
        else{
            if (vultable[board] == 0 || vultable[board] == 2) vul = 0;
            else vul = 1;            
            NSScoreconverter = -1;
        }
        ////////// overtricks
       // if (made.length() == 0) return 0; // passed out
       //if("".equals(made)) return 0; // passed out 
       
       
        if (made.startsWith("-")) ot = Integer.parseInt(made.substring(1)) * -1;
        else if (made.startsWith("=")) ot = 0;
        else ot = Integer.parseInt(made.substring(1));
        
        /////////// contract going down
        if (ot < 0){            
            if (dbl == 0) return (vul == 0) ? ot * 50 * NSScoreconverter : ot * 100 * NSScoreconverter ;
            
            if (vul == 0) score = (ot < -3) ? ((ot + 3) * 300) - 500 : (ot * 200) + 100;
            else score = ot * 300 + 100;
            score *= NSScoreconverter;
            return (dbl == 1) ? score : score * 2;           
        }        
        
        /////////// contract made
        
        if ("C".equals(strain) || "D".equals(strain)) denom = 1;
        else if ("H".equals(strain) || "S".equals(strain)) denom = 3;
        else denom = 5;        
       
        int unit = 30; // majors + NT
        if (denom < 3) unit = 20; //minors

        score = unit * level; 
        if (denom == 5) score += 10; //NT

        if(dbl == 1) score = score * 2;
        else if(dbl == 2) score = score * 4;

        if (score < 100) score += 50; //partial
        else {
             if (vul==1) score += 500;
             else score += 300;

             if (level == 6){
                 if (vul==1)score += 750;
                 else score += 500;
             }
             if (level == 7){
                 if (vul==1)score += 1500;
                 else score += 1000;
             }
        }

        switch (dbl) {
            case 0:
                score += ot * unit;
                break;
            case 1:
                score += ot * 2 * (1+vul) * 50;
                score += 50;
                break;
            case 2:
                score += ot * 4 * (1+vul) * 50;
                score += 100;	
                break;
            default:
                break;
        }

        return score*NSScoreconverter;
   }
}
