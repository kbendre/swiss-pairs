/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swisspairs;

/**
 *
 * @author Kaustubh Bendre
 */
public class RoundScoreSheet {
    public int rndno;
    public int tables;
    public ScoreSheet[] rss;
    
    public RoundScoreSheet RoundScoreSheet(){  
        return this; 
    }
    
    public void fill(int rno){
        tables = SwissPairs.tables;
        rss = new ScoreSheet[tables+1];
        rndno = rno;
    }
}
